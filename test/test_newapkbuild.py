# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import glob
import os
import pytest
import shutil
import sys

import sbb_test  # noqa
import sbb_test.const
import sbb.build.newapkbuild
import sbb.config
import sbb.config.init
import sbb.helpers.logging


@pytest.fixture
def args(tmpdir, request):
    import sbb.parse
    cfg = f"{sbb_test.const.testdata}/channels.cfg"
    sys.argv = ["sbootstrap.py", "--config-channels", cfg, "init"]
    args = sbb.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    sbb.helpers.logging.init(args)
    request.addfinalizer(args.logfd.close)
    return args


def test_newapkbuild(args, monkeypatch, tmpdir):
    testdata = sbb_test.const.testdata

    # Fake functions
    def confirm_true(*nargs):
        return True

    def confirm_false(*nargs):
        return False

    # Preparation
    monkeypatch.setattr(sbb.helpers.cli, "confirm", confirm_false)
    sbb.build.init(args)
    args.aports = tmpdir = str(tmpdir)
    shutil.copy(f"{testdata}/sbaports.cfg", args.aports)
    func = sbb.build.newapkbuild

    # Show the help
    func(args, "main", ["-h"])
    assert glob.glob(f"{tmpdir}/*") == [f"{tmpdir}/sbaports.cfg"]

    # Test package
    pkgname = "testpackage"
    func(args, "main", [pkgname])
    apkbuild_path = tmpdir + "/main/" + pkgname + "/APKBUILD"
    apkbuild = sbb.parse.apkbuild(args, apkbuild_path)
    assert apkbuild["pkgname"] == pkgname
    assert apkbuild["pkgdesc"] == ""

    # Don't overwrite
    with pytest.raises(RuntimeError) as e:
        func(args, "main", [pkgname])
    assert "Aborted" in str(e.value)

    # Overwrite
    monkeypatch.setattr(sbb.helpers.cli, "confirm", confirm_true)
    pkgdesc = "testdescription"
    func(args, "main", ["-d", pkgdesc, pkgname])
    args.cache["apkbuild"] = {}
    apkbuild = sbb.parse.apkbuild(args, apkbuild_path)
    assert apkbuild["pkgname"] == pkgname
    assert apkbuild["pkgdesc"] == pkgdesc

    # There should be no src folder
    assert not os.path.exists(tmpdir + "/main/" + pkgname + "/src")
