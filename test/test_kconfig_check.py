# Copyright 2021 Antoine Fontaine
# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import pytest
import sys

import sbb_test
import sbb_test.const
import sbb.parse.kconfig


@pytest.fixture
def args(tmpdir, request):
    import sbb.parse
    sys.argv = ["sbootstrap.py", "kconfig", "check"]
    args = sbb.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    sbb.helpers.logging.init(args)
    request.addfinalizer(args.logfd.close)
    return args


def test_kconfig_check(args):
    # basic checks, from easiers to hard-ish
    dir = f"{sbb_test.const.testdata}/kconfig_check/"
    assert not sbb.parse.kconfig.check_file(args, dir +
                                            "bad-missing-required-option")
    assert sbb.parse.kconfig.check_file(args, dir + "good")
    assert not sbb.parse.kconfig.check_file(args, dir + "bad-wrong-option-set")
    assert sbb.parse.kconfig.check_file(args, dir + "good-anbox",
                                        anbox=True)
    assert not sbb.parse.kconfig.check_file(args, dir +
                                            "bad-array-missing-some-options",
                                            anbox=True)
    assert sbb.parse.kconfig.check_file(args, dir + "good-nftables",
                                        nftables=True)
    assert not sbb.parse.kconfig.check_file(args, dir + "bad-nftables",
                                            nftables=True)
    assert sbb.parse.kconfig.check_file(args, dir + "good-zram",
                                        zram=True)

    # tests on real devices

    # it's a spacebus device, it will have the required options, and
    # supports nftables (with sbb:kconfigcheck-nftables)
    assert sbb.parse.kconfig.check(args, "nokia-n900")

    # supports Anbox (with sbb:kconfigcheck-anbox)
    assert sbb.parse.kconfig.check(args, "spacebus-allwinner")

    # testing the force param: nokia-n900 will never have anbox support
    assert not sbb.parse.kconfig.check(args, "nokia-n900",
                                       force_anbox_check=True)

    # supports zram (with sbb:kconfigcheck-zram), nftables
    assert sbb.parse.kconfig.check(args, "linux-purism-librem5")
