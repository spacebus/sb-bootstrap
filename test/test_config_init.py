# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import pytest

import sbb_test  # noqa
import sbb.config.init


def test_require_programs(monkeypatch):
    func = sbb.config.init.require_programs

    # Nothing missing
    func()

    # Missing program
    invalid = "invalid-program-name-here-asdf"
    monkeypatch.setattr(sbb.config, "required_programs", [invalid])
    with pytest.raises(RuntimeError) as e:
        func()
    assert str(e.value).startswith("Can't find all programs")
