# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import pytest
import sys

import sbb_test  # noqa
import sbb.build.other


@pytest.fixture
def args(request):
    import sbb.parse
    sys.argv = ["sbootstrap", "init"]
    args = sbb.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    sbb.helpers.logging.init(args)
    request.addfinalizer(args.logfd.close)
    return args


def test_filter_missing_packages_invalid(args):
    """ Test ...repo_missing.filter_missing_packages(): invalid package """
    func = sbb.helpers.repo_missing.filter_missing_packages
    with pytest.raises(RuntimeError) as e:
        func(args, "armhf", ["invalid-package-name"])
    assert str(e.value).startswith("Could not find aport")


def test_filter_missing_packages_binary_exists(args):
    """ Test ...repo_missing.filter_missing_packages(): binary exists """
    func = sbb.helpers.repo_missing.filter_missing_packages
    assert func(args, "armhf", ["busybox"]) == []


def test_filter_missing_packages_sbaports(args, monkeypatch):
    """ Test ...repo_missing.filter_missing_packages(): sbaports """
    build_is_necessary = None
    func = sbb.helpers.repo_missing.filter_missing_packages

    def stub(args, arch, sbaport):
        return build_is_necessary
    monkeypatch.setattr(sbb.build, "is_necessary", stub)

    build_is_necessary = True
    assert func(args, "x86_64", ["busybox", "hello-world"]) == ["hello-world"]

    build_is_necessary = False
    assert func(args, "x86_64", ["busybox", "hello-world"]) == []


def test_filter_aport_packages(args):
    """ Test ...repo_missing.filter_aport_packages() """
    func = sbb.helpers.repo_missing.filter_aport_packages
    assert func(args, "armhf", ["busybox", "hello-world"]) == ["hello-world"]


def test_filter_arch_packages(args, monkeypatch):
    """ Test ...repo_missing.filter_arch_packages() """
    func = sbb.helpers.repo_missing.filter_arch_packages
    check_arch = None

    def stub(args, arch, sbaport, binary=True):
        return check_arch
    monkeypatch.setattr(sbb.helpers.package, "check_arch", stub)

    check_arch = False
    assert func(args, "armhf", ["hello-world"]) == []

    check_arch = True
    assert func(args, "armhf", []) == []


def test_get_relevant_packages(args, monkeypatch):
    """ Test ...repo_missing.get_relevant_packages() """

    # Set up fake return values
    stub_data = {"check_arch": False,
                 "depends_recurse": ["a", "b", "c", "d"],
                 "filter_arch_packages": ["a", "b", "c"],
                 "filter_aport_packages": ["b", "a"],
                 "filter_missing_packages": ["a"]}

    def stub(args, arch, sbaport, binary=True):
        return stub_data["check_arch"]
    monkeypatch.setattr(sbb.helpers.package, "check_arch", stub)

    def stub(args, arch, sbaport):
        return stub_data["depends_recurse"]
    monkeypatch.setattr(sbb.helpers.package, "depends_recurse", stub)

    def stub(args, arch, sbaport):
        return stub_data["filter_arch_packages"]
    monkeypatch.setattr(sbb.helpers.repo_missing, "filter_arch_packages", stub)

    def stub(args, arch, sbaport):
        return stub_data["filter_aport_packages"]
    monkeypatch.setattr(sbb.helpers.repo_missing, "filter_aport_packages",
                        stub)

    def stub(args, arch, sbaport):
        return stub_data["filter_missing_packages"]
    monkeypatch.setattr(sbb.helpers.repo_missing, "filter_missing_packages",
                        stub)

    # No given package
    func = sbb.helpers.repo_missing.get_relevant_packages
    assert func(args, "armhf") == ["a"]
    assert func(args, "armhf", built=True) == ["a", "b"]

    # Package can't be built for given arch
    with pytest.raises(RuntimeError) as e:
        func(args, "armhf", "a")
    assert "can't be built" in str(e.value)

    # Package can be built for given arch
    stub_data["check_arch"] = True
    assert func(args, "armhf", "a") == ["a"]
    assert func(args, "armhf", "a", True) == ["a", "b"]


def test_generate_output_format(args, monkeypatch):
    """ Test ...repo_missing.generate_output_format() """

    def stub(args, pkgname, arch, replace_subpkgnames=False):
        return {"pkgname": "hello-world", "version": "1.0-r0",
                "depends": ["depend1", "depend2"]}
    monkeypatch.setattr(sbb.helpers.package, "get", stub)

    def stub(args, pkgname):
        return "main"
    monkeypatch.setattr(sbb.helpers.sbaports, "get_repo", stub)

    func = sbb.helpers.repo_missing.generate_output_format
    ret = [{"pkgname": "hello-world",
            "repo": "main",
            "version": "1.0-r0",
            "depends": ["depend1", "depend2"]}]
    assert func(args, "armhf", ["hello-world"]) == ret
