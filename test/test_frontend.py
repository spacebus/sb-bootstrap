# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import sys
import pytest

import sbb_test  # noqa
import sbb.config
import sbb.parse
import sbb.helpers.frontend
import sbb.helpers.logging


def test_build_src_invalid_path():
    sys.argv = ["sbootstrap.py", "build", "--src=/invalidpath", "hello-world"]
    args = sbb.parse.arguments()

    with pytest.raises(RuntimeError) as e:
        sbb.helpers.frontend.build(args)
    assert str(e.value).startswith("Invalid path specified for --src:")
