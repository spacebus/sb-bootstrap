# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import pytest
import sys

import sbb_test.const
import sbb.parse


@pytest.fixture
def args(tmpdir, request):
    import sbb.parse
    sys.argv = ["sbootstrap.py", "init"]
    args = sbb.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    sbb.helpers.logging.init(args)
    request.addfinalizer(args.logfd.close)
    return args


def test_kernel_suffix(args):
    args.aports = sbb_test.const.testdata + "/deviceinfo/aports"
    device = "multiple-kernels"

    kernel = "mainline"
    deviceinfo = sbb.parse.deviceinfo(args, device, kernel)
    assert deviceinfo["append_dtb"] == "yes"
    assert deviceinfo["dtb"] == "mainline-dtb"

    kernel = "mainline-modem"
    deviceinfo = sbb.parse.deviceinfo(args, device, kernel)
    assert deviceinfo["append_dtb"] == "yes"
    assert deviceinfo["dtb"] == "mainline-modem-dtb"

    kernel = "downstream"
    deviceinfo = sbb.parse.deviceinfo(args, device, kernel)
    assert deviceinfo["append_dtb"] == "yes"
    assert deviceinfo["dtb"] == "downstream-dtb"
