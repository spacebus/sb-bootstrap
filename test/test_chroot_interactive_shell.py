# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import subprocess
import os
import sbb_test  # noqa
import sbb.config


def test_chroot_interactive_shell():
    """
    Open a shell with 'sbootstrap chroot' and pass 'echo hello_world\n' as
    stdin.
    """
    os.chdir(sbb.config.sbb_src)
    ret = subprocess.check_output(["./sbootstrap.py", "-q", "chroot", "sh"],
                                  timeout=300, input="echo hello_world\n",
                                  universal_newlines=True,
                                  stderr=subprocess.STDOUT)
    assert ret == "hello_world\n"


def test_chroot_interactive_shell_user():
    """
    Open a shell with 'sbootstrap chroot' as user, and test the resulting ID.
    """
    os.chdir(sbb.config.sbb_src)
    ret = subprocess.check_output(["./sbootstrap.py", "-q", "chroot",
                                   "--user", "sh"], timeout=300,
                                  input="id -un",
                                  universal_newlines=True,
                                  stderr=subprocess.STDOUT)
    assert ret == "sbos\n"


def test_chroot_arguments():
    """
    Open a shell with 'sbootstrap chroot' for every architecture, pass
    'uname -m\n' as stdin and check the output
    """
    os.chdir(sbb.config.sbb_src)

    for arch in ["armhf", "aarch64", "x86_64"]:
        ret = subprocess.check_output(["./sbootstrap.py", "-q", "chroot",
                                       "-b", arch, "sh"],
                                      timeout=300,
                                      input="uname -m\n",
                                      universal_newlines=True,
                                      stderr=subprocess.STDOUT)
        if arch == "armhf":
            assert ret == "armv7l\n"
        else:
            assert ret == arch + "\n"
