# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
""" Test sbb.helper.pkgrel_bump """
import glob
import os
import pytest
import shutil
import sys

import sbb_test  # noqa
import sbb.helpers.pkgrel_bump
import sbb.helpers.logging


@pytest.fixture
def args(request):
    import sbb.parse
    sys.argv = ["sbootstrap.py", "chroot"]
    args = sbb.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    sbb.helpers.logging.init(args)
    request.addfinalizer(args.logfd.close)
    return args


def sbootstrap(args, tmpdir, parameters, zero_exit=True):
    """
    Helper function for running sbootstrap inside the fake work folder
    (created by setup() below) with the binary repo disabled and with the
    testdata configured as aports.

    :param parameters: what to pass to sbootstrap, e.g. ["build", "testlib"]
    :param zero_exit: expect sbootstrap to exit with 0 (no error)
    """
    # Run sbootstrap
    aports = tmpdir + "/_aports"
    config = tmpdir + "/_sbootstrap.cfg"

    # Copy .git dir to fake sbaports
    dot_git = tmpdir + "/_aports/.git"
    if not os.path.exists(dot_git):
        shutil.copytree(args.aports + "/.git", dot_git)

    try:
        sbb.helpers.run.user(args, ["./sbootstrap.py", "--work=" + tmpdir,
                                    "--mirror-sbOS=", "--aports=" + aports,
                                    "--config=" + config] + parameters,
                             working_dir=sbb.config.sbb_src)

    # Verify that it exits as desired
    except Exception as exc:
        if zero_exit:
            raise RuntimeError("sbootstrap failed") from exc
        else:
            return
    if not zero_exit:
        raise RuntimeError("Expected sbootstrap to fail, but it did not!")


def setup_work(args, tmpdir):
    """
    Create fake work folder in tmpdir with everything symlinked except for the
    built packages. The aports testdata gets copied to the tempfolder as
    well, so it can be modified during testing.
    """
    # Clean the chroots, and initialize the build chroot in the native chroot.
    # We do this before creating the fake work folder, because then all
    # packages are still present.
    os.chdir(sbb.config.sbb_src)
    sbb.helpers.run.user(args, ["./sbootstrap.py", "-y", "zap"])
    sbb.helpers.run.user(args, ["./sbootstrap.py", "build_init"])
    sbb.helpers.run.user(args, ["./sbootstrap.py", "shutdown"])

    # Link everything from work (except for "packages") to the tmpdir
    for path in glob.glob(args.work + "/*"):
        if os.path.basename(path) != "packages":
            sbb.helpers.run.user(args, ["ln", "-s", path, tmpdir + "/"])

    # Copy testdata and selected device aport
    for folder in ["device/testing", "main"]:
        sbb.helpers.run.user(args, ["mkdir", "-p", args.aports, tmpdir +
                                    "/_aports/" + folder])
    path_original = sbb.helpers.sbaports.find(args, f"device-{args.device}")
    sbb.helpers.run.user(args, ["cp", "-r", path_original,
                                f"{tmpdir}/_aports/device/testing"])
    for pkgname in ["testlib", "testapp", "testsubpkg"]:
        sbb.helpers.run.user(args, ["cp", "-r",
                                    "test/testdata/pkgrel_bump/aports/"
                                    f"{pkgname}",
                                    f"{tmpdir}/_aports/main/{pkgname}"])

    # Copy sbaports.cfg
    sbb.helpers.run.user(args, ["cp", args.aports + "/sbaports.cfg", tmpdir +
                                "/_aports"])

    # Empty packages folder
    channel = sbb.config.sbaports.read_config(args)["channel"]
    packages_path = f"{tmpdir}/packages/{channel}"
    sbb.helpers.run.user(args, ["mkdir", "-p", packages_path])
    sbb.helpers.run.user(args, ["chmod", "777", packages_path])

    # Copy over the sbootstrap config
    sbb.helpers.run.user(args, ["cp", args.config, tmpdir +
                                "/_sbootstrap.cfg"])


def verify_pkgrels(args, tmpdir, pkgrel_testlib, pkgrel_testapp,
                   pkgrel_testsubpkg):
    """
    Verify the pkgrels of the three test APKBUILDs ("testlib", "testapp",
    "testsubpkg").
    """
    args.cache["apkbuild"] = {}
    mapping = {"testlib": pkgrel_testlib,
               "testapp": pkgrel_testapp,
               "testsubpkg": pkgrel_testsubpkg}
    for pkgname, pkgrel in mapping.items():
        # APKBUILD path
        path = tmpdir + "/_aports/main/" + pkgname + "/APKBUILD"

        # Parse and verify
        apkbuild = sbb.parse.apkbuild(args, path)
        assert pkgrel == int(apkbuild["pkgrel"])


def test_pkgrel_bump_high_level(args, tmpdir):
    # Tempdir setup
    tmpdir = str(tmpdir)
    setup_work(args, tmpdir)

    # Let pkgrel_bump exit normally
    sbootstrap(args, tmpdir, ["build", "testlib", "testapp", "testsubpkg"])
    sbootstrap(args, tmpdir, ["pkgrel_bump", "--dry", "--auto"])
    verify_pkgrels(args, tmpdir, 0, 0, 0)

    # Increase soname (testlib soname changes with the pkgrel)
    sbootstrap(args, tmpdir, ["pkgrel_bump", "testlib"])
    verify_pkgrels(args, tmpdir, 1, 0, 0)
    sbootstrap(args, tmpdir, ["build", "testlib"])
    sbootstrap(args, tmpdir, ["pkgrel_bump", "--dry", "--auto"])
    verify_pkgrels(args, tmpdir, 1, 0, 0)

    # Delete package with previous soname (--auto-dry exits with >0 now)
    channel = sbb.config.sbaports.read_config(args)["channel"]
    arch = args.arch_native
    apk_path = f"{tmpdir}/packages/{channel}/{arch}/testlib-1.0-r0.apk"
    sbb.helpers.run.root(args, ["rm", apk_path])
    sbootstrap(args, tmpdir, ["index"])
    sbootstrap(args, tmpdir, ["pkgrel_bump", "--dry", "--auto"], False)
    verify_pkgrels(args, tmpdir, 1, 0, 0)

    # Bump pkgrel and build testapp/testsubpkg
    sbootstrap(args, tmpdir, ["pkgrel_bump", "--auto"])
    verify_pkgrels(args, tmpdir, 1, 1, 1)
    sbootstrap(args, tmpdir, ["build", "testapp", "testsubpkg"])

    # After rebuilding, pkgrel_bump --auto-dry exits with 0
    sbootstrap(args, tmpdir, ["pkgrel_bump", "--dry", "--auto"])
    verify_pkgrels(args, tmpdir, 1, 1, 1)

    # Test running with specific package names
    sbootstrap(args, tmpdir, ["pkgrel_bump", "invalid_package_name"], False)
    sbootstrap(args, tmpdir, ["pkgrel_bump", "--dry", "testlib"], False)
    verify_pkgrels(args, tmpdir, 1, 1, 1)

    # Clean up
    sbootstrap(args, tmpdir, ["shutdown"])
    sbb.helpers.run.root(args, ["rm", "-rf", tmpdir])
