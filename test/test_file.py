# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import sys
import time
import pytest

import sbb_test  # noqa
import sbb.helpers.git
import sbb.helpers.logging
import sbb.parse.version


@pytest.fixture
def args(request):
    import sbb.parse
    sys.argv = ["sbootstrap.py", "chroot"]
    args = sbb.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    sbb.helpers.logging.init(args)
    request.addfinalizer(args.logfd.close)
    return args


def test_file_is_older_than(args, tmpdir):
    # Create a file last modified 10s ago
    tempfile = str(tmpdir) + "/test"
    sbb.helpers.run.user(args, ["touch", tempfile])
    past = time.time() - 10
    os.utime(tempfile, (-1, past))

    # Check the bounds
    func = sbb.helpers.file.is_older_than
    assert func(tempfile, 9) is True
    assert func(tempfile, 10) is True
    assert func(tempfile, 11) is False
