# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import sys
import pytest

import sbb_test
import sbb_test.const
import sbb.chroot.apk_static
import sbb.parse.apkindex
import sbb.helpers.logging
import sbb.parse.bootimg


@pytest.fixture
def args(request):
    import sbb.parse
    sys.argv = ["sbootstrap.py", "chroot"]
    args = sbb.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    sbb.helpers.logging.init(args)
    request.addfinalizer(args.logfd.close)
    return args


def test_bootimg_invalid_path(args):
    with pytest.raises(RuntimeError) as e:
        sbb.parse.bootimg(args, "/invalid-path")
    assert "Could not find file" in str(e.value)


def test_bootimg_kernel(args):
    path = sbb_test.const.testdata + "/bootimg/kernel-boot.img"
    with pytest.raises(RuntimeError) as e:
        sbb.parse.bootimg(args, path)
    assert "heimdall-isorec" in str(e.value)


def test_bootimg_invalid_file(args):
    with pytest.raises(RuntimeError) as e:
        sbb.parse.bootimg(args, __file__)
    assert "File is not an Android boot.img" in str(e.value)


def test_bootimg_normal(args):
    path = sbb_test.const.testdata + "/bootimg/normal-boot.img"
    output = {"base": "0x80000000",
              "kernel_offset": "0x00008000",
              "ramdisk_offset": "0x04000000",
              "second_offset": "0x00f00000",
              "tags_offset": "0x0e000000",
              "pagesize": "2048",
              "cmdline": "bootopt=64S3,32S1,32S1",
              "qcdt": "false",
              "mtk_mkimage": "false",
              "dtb_second": "false"}
    assert sbb.parse.bootimg(args, path) == output


def test_bootimg_qcdt(args):
    path = sbb_test.const.testdata + "/bootimg/qcdt-boot.img"
    output = {"base": "0x80000000",
              "kernel_offset": "0x00008000",
              "ramdisk_offset": "0x04000000",
              "second_offset": "0x00f00000",
              "tags_offset": "0x0e000000",
              "pagesize": "2048",
              "cmdline": "bootopt=64S3,32S1,32S1",
              "qcdt": "true",
              "mtk_mkimage": "false",
              "dtb_second": "false"}
    assert sbb.parse.bootimg(args, path) == output


def test_bootimg_mtk_mkimage(args):
    path = sbb_test.const.testdata + "/bootimg/mtk_mkimage-boot.img"
    output = {"base": "0x80000000",
              "kernel_offset": "0x00008000",
              "ramdisk_offset": "0x04000000",
              "second_offset": "0x00f00000",
              "tags_offset": "0x0e000000",
              "pagesize": "2048",
              "cmdline": "bootopt=64S3,32S1,32S1",
              "qcdt": "false",
              "mtk_mkimage": "true",
              "dtb_second": "false"}
    assert sbb.parse.bootimg(args, path) == output


def test_bootimg_dtb_second(args):
    path = sbb_test.const.testdata + "/bootimg/dtb-second-boot.img"
    output = {"base": "0x00000000",
              "kernel_offset": "0x00008000",
              "ramdisk_offset": "0x02000000",
              "second_offset": "0x00f00000",
              "tags_offset": "0x00000100",
              "pagesize": "2048",
              "cmdline": "bootopt=64S3,32S1,32S1",
              "qcdt": "false",
              "mtk_mkimage": "false",
              "dtb_second": "true"}
    assert sbb.parse.bootimg(args, path) == output
