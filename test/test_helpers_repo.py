# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
""" Test sbb.helpers.repo """
import pytest
import sys

import sbb_test  # noqa
import sbb_test.const
import sbb.helpers.repo


@pytest.fixture
def args(tmpdir, request):
    import sbb.parse
    cfg = f"{sbb_test.const.testdata}/channels.cfg"
    sys.argv = ["sbootstrap.py", "--config-channels", cfg, "chroot"]
    args = sbb.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    sbb.helpers.logging.init(args)
    request.addfinalizer(args.logfd.close)
    return args


def test_hash():
    url = "https://nl.alpinelinux.org/alpine/edge/testing"
    hash = "865a153c"
    assert sbb.helpers.repo.hash(url, 8) == hash


def test_alpine_apkindex_path(args):
    func = sbb.helpers.repo.alpine_apkindex_path
    args.mirror_alpine = "http://dl-cdn.alpinelinux.org/alpine/"
    ret = args.work + "/cache_apk_armhf/APKINDEX.30e6f5af.tar.gz"
    assert func(args, "testing", "armhf") == ret


def test_urls(args, monkeypatch):
    func = sbb.helpers.repo.urls
    channel = "v20.05"
    args.mirror_alpine = "http://localhost/alpine/"

    # Second mirror with /master at the end is legacy, gets fixed by func.
    # Note that bpo uses multiple spacebus mirrors at the same time, so it
    # can use its WIP repository together with the final repository.
    args.mirrors_spacebus = ["http://localhost/sbos1/",
                                 "http://localhost/sbos2/master"]

    # Pretend to have a certain channel in sbaports.cfg
    def read_config(args):
        return {"channel": channel}
    monkeypatch.setattr(sbb.config.sbaports, "read_config", read_config)

    # Channel: v20.05
    assert func(args) == ["/mnt/sbootstrap-packages",
                          "http://localhost/sbos1/v20.05",
                          "http://localhost/sbos2/v20.05",
                          "http://localhost/alpine/v3.11/main",
                          "http://localhost/alpine/v3.11/community"]

    # Channel: edge (has Alpine's testing)
    channel = "edge"
    assert func(args) == ["/mnt/sbootstrap-packages",
                          "http://localhost/sbos1/master",
                          "http://localhost/sbos2/master",
                          "http://localhost/alpine/edge/main",
                          "http://localhost/alpine/edge/community",
                          "http://localhost/alpine/edge/testing"]

    # Only Alpine's URLs
    exp = ["http://localhost/alpine/edge/main",
           "http://localhost/alpine/edge/community",
           "http://localhost/alpine/edge/testing"]
    assert func(args, False, False) == exp
