# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import pytest
import sys

import sbb_test
import sbb_test.const
import sbb.helpers.logging
import sbb.helpers.ui


@pytest.fixture
def args(tmpdir, request):
    import sbb.parse
    cfg = f"{sbb_test.const.testdata}/channels.cfg"
    sys.argv = ["sbootstrap.py", "--config-channels", cfg, "init"]
    args = sbb.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    sbb.helpers.logging.init(args)
    request.addfinalizer(args.logfd.close)
    return args


def test_helpers_ui(args):
    """ Test the UIs returned by sbb.helpers.ui.list() with a testdata sbaports
        dir. That test dir has a plasma-mobile UI, which is disabled for armhf,
        so it must not be returned when querying the UI list for armhf. """
    args.aports = f"{sbb_test.const.testdata}/helpers_ui/sbaports"
    func = sbb.helpers.ui.list
    assert func(args, "armhf") == [("none", "No graphical environment")]
    assert func(args, "x86_64") == [("none", "No graphical environment"),
                                    ("plasma-mobile", "cool pkgdesc")]
