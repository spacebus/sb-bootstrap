# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import sbb.config


testdata = sbb.config.sbb_src + "/test/testdata"
