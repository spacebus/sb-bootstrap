# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import sys
import pytest
import shutil
import filecmp

import sbb_test
import sbb_test.const
import sbb.aportgen
import sbb.aportgen.core
import sbb.config
import sbb.helpers.logging


@pytest.fixture
def args(tmpdir, request):
    import sbb.parse
    cfg = f"{sbb_test.const.testdata}/channels.cfg"
    sys.argv = ["sbootstrap.py", "--config-channels", cfg, "chroot"]
    args = sbb.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    args.fork_alpine = False
    sbb.helpers.logging.init(args)
    request.addfinalizer(args.logfd.close)
    return args


def test_aportgen_compare_output(args, tmpdir, monkeypatch):
    # Fake aports folder in tmpdir
    tmpdir = str(tmpdir)
    shutil.copytree(args.aports + "/.git", tmpdir + "/.git")
    args.aports = tmpdir
    os.mkdir(tmpdir + "/cross")
    testdata = sbb_test.const.testdata + "/aportgen"

    # Override get_upstream_aport() to point to testdata
    def func(args, upstream_path, arch=None):
        return testdata + "/aports/main/" + upstream_path
    monkeypatch.setattr(sbb.aportgen.core, "get_upstream_aport", func)

    # Run aportgen and compare output
    pkgnames = ["binutils-armhf", "gcc-armhf"]
    for pkgname in pkgnames:
        sbb.aportgen.generate(args, pkgname)
        path_new = args.aports + "/cross/" + pkgname + "/APKBUILD"
        path_old = testdata + "/sbaports/cross/" + pkgname + "/APKBUILD"
        assert os.path.exists(path_new)
        assert filecmp.cmp(path_new, path_old, False)


def test_aportgen_fork_alpine_compare_output(args, tmpdir, monkeypatch):
    # Fake aports folder in tmpdir
    tmpdir = str(tmpdir)
    shutil.copytree(args.aports + "/.git", tmpdir + "/.git")
    args.aports = tmpdir
    os.mkdir(tmpdir + "/temp")
    testdata = sbb_test.const.testdata + "/aportgen"
    args.fork_alpine = True

    # Override get_upstream_aport() to point to testdata
    def func(args, upstream_path, arch=None):
        return testdata + "/aports/main/" + upstream_path
    monkeypatch.setattr(sbb.aportgen.core, "get_upstream_aport", func)

    # Run aportgen and compare output
    pkgname = "binutils"
    sbb.aportgen.generate(args, pkgname)
    path_new = args.aports + "/temp/" + pkgname + "/APKBUILD"
    path_old = testdata + "/sbaports/temp/" + pkgname + "/APKBUILD"
    assert os.path.exists(path_new)
    assert filecmp.cmp(path_new, path_old, False)


def test_aportgen(args, tmpdir):
    # Fake aports folder in tmpdir
    testdata = sbb_test.const.testdata
    tmpdir = str(tmpdir)
    shutil.copytree(args.aports + "/.git", tmpdir + "/.git")
    args.aports = tmpdir
    shutil.copy(f"{testdata}/sbaports.cfg", args.aports)
    os.mkdir(tmpdir + "/cross")

    # Create aportgen folder -> code path where it still exists
    sbb.helpers.run.user(args, ["mkdir", "-p", args.work + "/aportgen"])

    # Generate all valid packages (gcc twice -> different code path)
    pkgnames = ["binutils-armv7", "musl-armv7", "busybox-static-armv7",
                "gcc-armv7", "gcc-armv7"]
    for pkgname in pkgnames:
        sbb.aportgen.generate(args, pkgname)


def test_aportgen_invalid_generator(args):
    with pytest.raises(ValueError) as e:
        sbb.aportgen.generate(args, "pkgname-with-no-generator")
    assert "No generator available" in str(e.value)


def test_aportgen_get_upstream_aport(args, monkeypatch):
    # Fake sbb.parse.apkbuild()
    def fake_apkbuild(*args, **kwargs):
        return apkbuild
    monkeypatch.setattr(sbb.parse, "apkbuild", fake_apkbuild)

    # Fake sbb.parse.apkindex.package()
    def fake_package(*args, **kwargs):
        return package
    monkeypatch.setattr(sbb.parse.apkindex, "package", fake_package)

    # Equal version
    func = sbb.aportgen.core.get_upstream_aport
    upstream = "gcc"
    upstream_full = args.work + "/cache_git/aports_upstream/main/" + upstream
    apkbuild = {"pkgver": "2.0", "pkgrel": "0"}
    package = {"version": "2.0-r0"}
    assert func(args, upstream) == upstream_full

    # APKBUILD < binary
    apkbuild = {"pkgver": "1.0", "pkgrel": "0"}
    package = {"version": "2.0-r0"}
    with pytest.raises(RuntimeError) as e:
        func(args, upstream)
    assert str(e.value).startswith("You can update your local checkout with")

    # APKBUILD > binary
    apkbuild = {"pkgver": "3.0", "pkgrel": "0"}
    package = {"version": "2.0-r0"}
    with pytest.raises(RuntimeError) as e:
        func(args, upstream)
    assert str(e.value).startswith("You can force an update of your binary")
