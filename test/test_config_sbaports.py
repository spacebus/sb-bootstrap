# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
""" Test sbb/config/sbaports.py """
import pytest
import sys

import sbb_test
import sbb_test.const
import sbb_test.git
import sbb.config
import sbb.config.workdir
import sbb.config.sbaports


@pytest.fixture
def args(request):
    import sbb.parse
    cfg = f"{sbb_test.const.testdata}/channels.cfg"
    sys.argv = ["sbootstrap.py", "--config-channels", cfg, "init"]
    args = sbb.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    sbb.helpers.logging.init(args)
    request.addfinalizer(args.logfd.close)
    return args


def test_switch_to_channel_branch(args, monkeypatch, tmpdir):
    path, run_git = sbb_test.git.prepare_tmpdir(args, monkeypatch, tmpdir)
    args.aports = path

    # Pretend to have channel=edge in sbaports.cfg
    def read_config(args):
        return {"channel": "edge"}
    monkeypatch.setattr(sbb.config.sbaports, "read_config", read_config)

    # Success: Channel does not change
    func = sbb.config.sbaports.switch_to_channel_branch
    assert func(args, "edge") is False

    # Fail: git error (could be any error, but here: branch does not exist)
    with pytest.raises(RuntimeError) as e:
        func(args, "v20.05")
    assert str(e.value).startswith("Failed to switch branch")

    # Success: switch channel and change branch
    run_git(["checkout", "-b", "v20.05"])
    run_git(["checkout", "master"])
    assert func(args, "v20.05") is True
    branch = sbb.helpers.git.rev_parse(args, path, extra_args=["--abbrev-ref"])
    assert branch == "v20.05"


def test_read_config_channel(args, monkeypatch):
    channel = "edge"

    # Pretend to have a certain channel in sbaports.cfg
    def read_config(args):
        return {"channel": channel}
    monkeypatch.setattr(sbb.config.sbaports, "read_config", read_config)

    # Channel found
    func = sbb.config.sbaports.read_config_channel
    exp = {"description": "Rolling release channel",
           "branch_sbaports": "master",
           "branch_aports": "master",
           "mirrordir_alpine": "edge"}
    assert func(args) == exp

    # Channel not found
    channel = "non-existing"
    with pytest.raises(RuntimeError) as e:
        func(args)
    assert "channel was not found in channels.cfg" in str(e.value)
