# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import pytest
import sys

import sbb_test
import sbb_test.const
import sbb.parse._apkbuild


@pytest.fixture
def args(tmpdir, request):
    import sbb.parse
    sys.argv = ["sbootstrap.py", "init"]
    args = sbb.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    sbb.helpers.logging.init(args)
    request.addfinalizer(args.logfd.close)
    return args


def test_subpackages(args):
    testdata = sbb_test.const.testdata
    path = testdata + "/apkbuild/APKBUILD.subpackages"
    apkbuild = sbb.parse.apkbuild(args, path, check_pkgname=False)

    subpkg = apkbuild["subpackages"]["simple"]
    assert subpkg["pkgdesc"] == ""
    # Inherited from parent package
    assert subpkg["depends"] == ["spacebus-base"]

    subpkg = apkbuild["subpackages"]["custom"]
    assert subpkg["pkgdesc"] == "This is one of the custom subpackages"
    assert subpkg["depends"] == ["spacebus-base", "glibc"]

    # Successful extraction
    path = (testdata + "/init_questions_device/aports/device/testing/"
            "device-nonfree-firmware/APKBUILD")
    apkbuild = sbb.parse.apkbuild(args, path)
    subpkg = (apkbuild["subpackages"]
              ["device-nonfree-firmware-nonfree-firmware"])
    assert subpkg["pkgdesc"] == "firmware description"

    # Can't find the pkgdesc in the function
    path = testdata + "/apkbuild/APKBUILD.missing-pkgdesc-in-subpackage"
    apkbuild = sbb.parse.apkbuild(args, path, check_pkgname=False)
    subpkg = (apkbuild["subpackages"]
              ["missing-pkgdesc-in-subpackage-subpackage"])
    assert subpkg["pkgdesc"] == ""

    # Can't find the function
    assert apkbuild["subpackages"]["invalid-function"] is None


def test_kernels(args):
    # Kernel hardcoded in depends
    args.aports = sbb_test.const.testdata + "/init_questions_device/aports"
    func = sbb.parse._apkbuild.kernels
    device = "lg-mako"
    assert func(args, device) is None

    # Upstream and downstream kernel
    device = "sony-amami"
    ret = {"downstream": "Downstream description",
           "mainline": "Mainline description"}
    assert func(args, device) == ret

    # Long kernel name (e.g. two different mainline kernels)
    device = "wileyfox-crackling"
    ret = {"mainline": "Mainline kernel (no modem)",
           "mainline-modem": "Mainline kernel (with modem)",
           "downstream": "Downstream kernel"}
    assert func(args, device) == ret


def test_depends_in_depends(args):
    path = sbb_test.const.testdata + "/apkbuild/APKBUILD.depends-in-depends"
    apkbuild = sbb.parse.apkbuild(args, path, check_pkgname=False)
    assert apkbuild["depends"] == ["first", "second", "third"]


def test_parse_attributes():
    # Convenience function for calling the function with a block of text
    def func(attribute, block):
        lines = block.split("\n")
        for i in range(0, len(lines)):
            lines[i] += "\n"
        i = 0
        path = "(testcase in " + __file__ + ")"
        print("=== parsing attribute '" + attribute + "' in test block:")
        print(block)
        print("===")
        return sbb.parse._apkbuild.parse_attribute(attribute, lines, i, path)

    assert func("depends", "pkgname='test'") == (False, None, 0)

    assert func("pkgname", 'pkgname="test"') == (True, "test", 0)

    assert func("pkgname", "pkgname='test'") == (True, "test", 0)

    assert func("pkgname", "pkgname=test") == (True, "test", 0)

    assert func("pkgname", 'pkgname="test\n"') == (True, "test", 1)

    assert func("pkgname", 'pkgname="\ntest\n"') == (True, "test", 2)

    assert func("pkgname", 'pkgname="test" # random comment\npkgrel=3') == \
        (True, "test", 0)

    assert func("depends", "depends='\nfirst\nsecond\nthird\n'#") == \
        (True, "first second third", 4)

    assert func("depends", 'depends="\nfirst\n\tsecond third"') == \
        (True, "first second third", 2)

    assert func("depends", 'depends=') == (True, "", 0)

    with pytest.raises(RuntimeError) as e:
        func("depends", 'depends="\nmissing\nend\nquote\nsign')
    assert str(e.value).startswith("Can't find closing")

    with pytest.raises(RuntimeError) as e:
        func("depends", 'depends="')
    assert str(e.value).startswith("Can't find closing")


def test_variable_replacements(args):
    path = sbb_test.const.testdata + "/apkbuild/APKBUILD.variable-replacements"
    apkbuild = sbb.parse.apkbuild(args, path, check_pkgname=False)
    assert apkbuild["pkgdesc"] == "this should not affect variable replacement"
    assert apkbuild["url"] == "replacements variable string-replacements"
    assert list(apkbuild["subpackages"].keys()) == ["replacements", "test"]

    assert apkbuild["subpackages"]["replacements"] is None
    test_subpkg = apkbuild["subpackages"]["test"]
    assert test_subpkg["pkgdesc"] == ("this should not affect variable "
                                      "replacement")


def test_parse_maintainers(args):
    path = sbb_test.const.testdata + "/apkbuild/APKBUILD.lint"
    maintainers = [
        "Oliver Smith <ollieparanoid@spacebus.org>",
        "Hello World <hello@world>"
    ]

    assert sbb.parse._apkbuild.maintainers(path) == maintainers


def test_parse_unmaintained(args):
    path = (f"{sbb_test.const.testdata}/apkbuild"
            "/APKBUILD.missing-pkgdesc-in-subpackage")
    assert sbb.parse._apkbuild.unmaintained(path) == "This is broken!"
