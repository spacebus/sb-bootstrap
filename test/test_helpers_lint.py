# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import pytest
import shutil
import sys

import sbb_test
import sbb_test.const
import sbb.helpers.lint
import sbb.helpers.run


@pytest.fixture
def args(request):
    import sbb.parse
    sys.argv = ["sbootstrap", "lint"]
    args = sbb.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    sbb.helpers.logging.init(args)
    request.addfinalizer(args.logfd.close)
    return args


def test_sbootstrap_lint(args, tmpdir):
    args.aports = tmpdir = str(tmpdir)

    # Create hello-world sbaport in tmpdir
    apkbuild_orig = f"{sbb_test.const.testdata}/apkbuild/APKBUILD.lint"
    apkbuild_tmp = f"{tmpdir}/hello-world/APKBUILD"
    os.makedirs(f"{tmpdir}/hello-world")
    shutil.copyfile(apkbuild_orig, apkbuild_tmp)

    # Lint passes
    assert sbb.helpers.lint.check(args, "hello-world") == ""

    # Change "sbb:cross-native" to non-existing "sbb:invalid-opt"
    sbb.helpers.run.user(args, ["sed", "s/sbb:cross-native/sbb:invalid-opt/g",
                                "-i", apkbuild_tmp])

    # Lint error
    err_str = "invalid option 'sbb:invalid-opt'"
    assert err_str in sbb.helpers.lint.check(args, "hello-world")
