# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import sys
import pytest

import sbb_test  # noqa
import sbb.helpers.logging
import sbb.helpers.package


@pytest.fixture
def args(request):
    import sbb.parse
    sys.argv = ["sbootstrap", "init"]
    args = sbb.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    sbb.helpers.logging.init(args)
    request.addfinalizer(args.logfd.close)
    return args


def test_helpers_package_get_sbaports_and_cache(args, monkeypatch):
    """ Test sbb.helpers.package.get(): find in sbaports, use cached result """

    # Fake APKBUILD data
    def stub(args, pkgname, must_exist):
        return {"arch": ["armv7"],
                "depends": ["testdepend"],
                "pkgname": "testpkgname",
                "provides": ["testprovide"],
                "options": [],
                "checkdepends": [],
                "subpackages": {},
                "makedepends": [],
                "pkgver": "1.0",
                "pkgrel": "1"}
    monkeypatch.setattr(sbb.helpers.sbaports, "get", stub)

    package = {"arch": ["armv7"],
               "depends": ["testdepend"],
               "pkgname": "testpkgname",
               "provides": ["testprovide"],
               "version": "1.0-r1"}
    func = sbb.helpers.package.get
    assert func(args, "testpkgname", "armv7") == package

    # Cached result
    monkeypatch.delattr(sbb.helpers.sbaports, "get")
    assert func(args, "testpkgname", "armv7") == package


def test_helpers_package_get_apkindex(args, monkeypatch):
    """ Test sbb.helpers.package.get(): find in apkindex """

    # Fake APKINDEX data
    fake_apkindex_data = {"arch": "armv7",
                          "depends": ["testdepend"],
                          "pkgname": "testpkgname",
                          "provides": ["testprovide"],
                          "version": "1.0-r1"}

    def stub(args, pkgname, arch, must_exist):
        if arch != fake_apkindex_data["arch"]:
            return None
        return fake_apkindex_data
    monkeypatch.setattr(sbb.parse.apkindex, "package", stub)

    # Given arch
    package = {"arch": ["armv7"],
               "depends": ["testdepend"],
               "pkgname": "testpkgname",
               "provides": ["testprovide"],
               "version": "1.0-r1"}
    func = sbb.helpers.package.get
    assert func(args, "testpkgname", "armv7") == package

    # Other arch
    assert func(args, "testpkgname", "x86_64") == package


def test_helpers_package_depends_recurse(args):
    """ Test sbb.helpers.package.depends_recurse() """

    # Put fake data into the sbb.helpers.package.get() cache
    cache = {"a": {False: {"pkgname": "a", "depends": ["b", "c"]}},
             "b": {False: {"pkgname": "b", "depends": []}},
             "c": {False: {"pkgname": "c", "depends": ["d"]}},
             "d": {False: {"pkgname": "d", "depends": ["b"]}}}
    args.cache["sbb.helpers.package.get"]["armhf"] = cache

    # Normal runs
    func = sbb.helpers.package.depends_recurse
    assert func(args, "a", "armhf") == ["a", "b", "c", "d"]
    assert func(args, "d", "armhf") == ["b", "d"]

    # Cached result
    args.cache["sbb.helpers.package.get"]["armhf"] = {}
    assert func(args, "d", "armhf") == ["b", "d"]


def test_helpers_package_check_arch_package(args):
    """ Test sbb.helpers.package.check_arch(): binary = True """
    # Put fake data into the sbb.helpers.package.get() cache
    func = sbb.helpers.package.check_arch
    cache = {"a": {False: {"arch": []}}}
    args.cache["sbb.helpers.package.get"]["armhf"] = cache

    cache["a"][False]["arch"] = ["all !armhf"]
    assert func(args, "a", "armhf") is False

    cache["a"][False]["arch"] = ["all"]
    assert func(args, "a", "armhf") is True

    cache["a"][False]["arch"] = ["noarch"]
    assert func(args, "a", "armhf") is True

    cache["a"][False]["arch"] = ["armhf"]
    assert func(args, "a", "armhf") is True

    cache["a"][False]["arch"] = ["aarch64"]
    assert func(args, "a", "armhf") is False


def test_helpers_package_check_arch_sbaports(args, monkeypatch):
    """ Test sbb.helpers.package.check_arch(): binary = False """
    func = sbb.helpers.package.check_arch
    fake_sbaport = {"arch": []}

    def fake_sbaports_get(args, pkgname, must_exist=False):
        return fake_sbaport
    monkeypatch.setattr(sbb.helpers.sbaports, "get", fake_sbaports_get)

    fake_sbaport["arch"] = ["armhf"]
    assert func(args, "a", "armhf", False) is True

    fake_sbaport["arch"] = ["all", "!armhf"]
    assert func(args, "a", "armhf", False) is False
