# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import sys
import pytest

import sbb_test  # noqa
import sbb.aportgen
import sbb.config
import sbb.helpers.frontend
import sbb.helpers.logging
import sbb.helpers.run


@pytest.fixture
def args(tmpdir, request):
    import sbb.parse
    sys.argv = ["sbootstrap.py", "chroot"]
    args = sbb.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    sbb.helpers.logging.init(args)
    request.addfinalizer(args.logfd.close)
    return args


def change_config(monkeypatch, path_config, key, value):
    args = args_patched(monkeypatch, ["sbootstrap.py", "-c", path_config,
                                      "config", key, value])
    sbb.helpers.frontend.config(args)


def args_patched(monkeypatch, argv):
    monkeypatch.setattr(sys, "argv", argv)
    return sbb.parse.arguments()


def test_config_user(args, tmpdir, monkeypatch):
    # Temporary paths
    tmpdir = str(tmpdir)
    path_work = tmpdir + "/work"
    path_config = tmpdir + "/sbootstrap.cfg"

    # Generate default config (only uses tmpdir)
    cmd = sbb.helpers.run.flat_cmd(["./sbootstrap.py",
                                    "-c", path_config,
                                    "-w", path_work,
                                    "--aports", args.aports,
                                    "init"])
    sbb.helpers.run.user(args, ["sh", "-c", "yes '' | " + cmd],
                         sbb.config.sbb_src)

    # Load and verify default config
    argv = ["sbootstrap.py", "-c", path_config, "config"]
    args_default = args_patched(monkeypatch, argv)
    assert args_default.work == path_work

    # Modify jobs count
    change_config(monkeypatch, path_config, "jobs", "9000")
    assert args_patched(monkeypatch, argv).jobs == "9000"

    # Override jobs count via commandline (-j)
    argv_jobs = ["sbootstrap.py", "-c", path_config, "-j", "1000", "config"]
    assert args_patched(monkeypatch, argv_jobs).jobs == "1000"

    # Override a config option with something that evaluates to false
    argv_empty = ["sbootstrap.py", "-c", path_config, "-w", "",
                  "--details-to-stdout", "config"]
    assert args_patched(monkeypatch, argv_empty).work == ""
