# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import sys
import pytest

import sbb_test  # noqa
import sbb.helpers.logging
import sbb.helpers.sbaports


@pytest.fixture
def args(request, tmpdir):
    import sbb.parse
    sys.argv = ["sbootstrap.py", "chroot"]
    args = sbb.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    sbb.helpers.logging.init(args)
    request.addfinalizer(args.logfd.close)

    # Create an empty APKINDEX.tar.gz file, so we can use its path and
    # timestamp to put test information in the cache.
    apkindex_path = str(tmpdir) + "/APKINDEX.tar.gz"
    open(apkindex_path, "a").close()
    lastmod = os.path.getmtime(apkindex_path)
    args.cache["apkindex"][apkindex_path] = {"lastmod": lastmod,
                                             "multiple": {}}
    return args


def cache_apkindex(args, version):
    """
    Modify the cache of the parsed binary package repository's APKINDEX
    for the "hello-world" package.
    :param version: full version string, includes pkgver and pkgrl (e.g. 1-r2)
    """
    apkindex_path = list(args.cache["apkindex"].keys())[0]

    providers = args.cache[
        "apkindex"][apkindex_path]["multiple"]["hello-world"]
    providers["hello-world"]["version"] = version


def test_build_is_necessary(args):
    # Prepare APKBUILD and APKINDEX data
    aport = sbb.helpers.sbaports.find(args, "hello-world")
    apkbuild = sbb.parse.apkbuild(args, aport + "/APKBUILD")
    apkbuild["pkgver"] = "1"
    apkbuild["pkgrel"] = "2"
    indexes = list(args.cache["apkindex"].keys())
    apkindex_path = indexes[0]
    cache = {"hello-world": {"hello-world": {"pkgname": "hello-world",
                                             "version": "1-r2"}}}
    args.cache["apkindex"][apkindex_path]["multiple"] = cache

    # Binary repo has a newer version
    cache_apkindex(args, "999-r1")
    assert sbb.build.is_necessary(args, None, apkbuild, indexes) is False

    # Aports folder has a newer version
    cache_apkindex(args, "0-r0")
    assert sbb.build.is_necessary(args, None, apkbuild, indexes) is True

    # Same version
    cache_apkindex(args, "1-r2")
    assert sbb.build.is_necessary(args, None, apkbuild, indexes) is False


def test_build_is_necessary_no_binary_available(args):
    """
    APKINDEX cache is set up to fake an empty APKINDEX, which means, that the
    hello-world package has not been built yet.
    """
    indexes = list(args.cache["apkindex"].keys())
    aport = sbb.helpers.sbaports.find(args, "hello-world")
    apkbuild = sbb.parse.apkbuild(args, aport + "/APKBUILD")
    assert sbb.build.is_necessary(args, None, apkbuild, indexes) is True


def test_build_is_necessary_cant_build_sbaport_for_arch(args):
    """ sbaport version is higher than Alpine's binary package, but sbaport
        can't be built for given arch. (#1897) """

    apkbuild = {"pkgname": "alpine-base",
                "arch": "armhf",  # can't build for x86_64!
                "pkgver": "9999",
                "pkgrel": "0"}
    assert sbb.build.is_necessary(args, "x86_64", apkbuild) is False
    assert sbb.build.is_necessary(args, "armhf", apkbuild) is True
