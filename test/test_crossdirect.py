# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import pytest
import sys

import sbb_test  # noqa
import sbb.chroot.apk_static
import sbb.config.sbaports
import sbb.parse.apkindex
import sbb.helpers.logging
import sbb.helpers.run
import sbb.parse.bootimg


@pytest.fixture
def args(request):
    import sbb.parse
    sys.argv = ["sbootstrap.py", "chroot"]
    args = sbb.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    sbb.helpers.logging.init(args)
    request.addfinalizer(args.logfd.close)
    return args


def sbootstrap_run(args, parameters, check=True):
    """Execute sbootstrap.py with a test sbootstrap.conf."""
    return sbb.helpers.run.user(args, ["./sbootstrap.py"] + parameters,
                                working_dir=sbb.config.sbb_src,
                                check=check)


def test_crossdirect_rust(args):
    """ Set up buildroot_armv7 chroot for building, but remove /usr/bin/rustc.
        Build hello-world-rust for armv7, to verify that it uses
        /native/usr/bin/rustc instead of /usr/bin/rustc. The package has a
        check() function, which makes sure that the built program is actually
        working. """
    sbootstrap_run(args, ["-y", "zap"])

    # Remember previously selected device
    cfg = sbb.config.load(args)
    old_device = cfg['sbootstrap']['device']

    try:
        # First, switch to device that is known to exist on all channels,
        # such as qemu-amd64. Currently selected device may not exist in
        # stable branch!
        cfg['sbootstrap']['device'] = 'qemu-amd64'
        sbb.config.save(args, cfg)

        # Switch to "v20.05" channel, as a stable release of alpine is more
        # likely to have the same rustc version across various architectures.
        # If armv7/x86_64 have a different rustc version, this test will fail:
        # 'found crate `std` compiled by an incompatible version of rustc'
        sbb.config.sbaports.switch_to_channel_branch(args, "v21.03")

        sbootstrap_run(args, ["build_init", "-barmv7"])
        sbootstrap_run(args, ["chroot", "--add=rust", "-barmv7", "--",
                               "mv", "/usr/bin/rustc", "/usr/bin/rustc_"])
        sbootstrap_run(args, ["build", "hello-world-rust", "--arch=armv7",
                               "--force"])
        # Make /native/usr/bin/rustc unusuable too, to make the build fail
        sbootstrap_run(args, ["chroot", "--", "rm", "/usr/bin/rustc"])
        assert sbootstrap_run(args, ["build", "hello-world-rust",
                                      "--arch=armv7", "--force"],
                               check=False) == 1

        # Make /usr/bin/rustc usable again, to test fallback with qemu
        sbootstrap_run(args, ["chroot", "-barmv7", "--",
                               "mv", "/usr/bin/rustc_", "/usr/bin/rustc"])
        sbootstrap_run(args, ["build", "hello-world-rust", "--arch=armv7",
                               "--force"])
    finally:
        # Clean up
        sbb.config.sbaports.switch_to_channel_branch(args, "edge")
        sbootstrap_run(args, ["-y", "zap"])

        # Restore previously selected device
        cfg['sbootstrap']['device'] = old_device
        sbb.config.save(args, cfg)
