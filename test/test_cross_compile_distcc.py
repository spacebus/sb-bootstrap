# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import pytest
import sys

import sbb_test  # noqa
import sbb.build
import sbb.chroot.distccd
import sbb.helpers.logging


@pytest.fixture
def args(tmpdir, request):
    import sbb.parse
    sys.argv = ["sbootstrap", "init"]
    args = sbb.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    sbb.helpers.logging.init(args)
    request.addfinalizer(args.logfd.close)
    return args


def test_cross_compile_distcc(args):
    # Delete old distccd log
    sbb.chroot.distccd.stop(args)
    distccd_log = args.work + "/chroot_native/home/sbos/distccd.log"
    if os.path.exists(distccd_log):
        sbb.helpers.run.root(args, ["rm", distccd_log])

    # Force usage of distcc (no fallback, no ccache)
    args.verbose = True
    args.ccache = False
    args.distcc_fallback = False

    # Compile, print distccd and sshd logs on error
    try:
        sbb.build.package(args, "hello-world", arch="armhf", force=True)
    except RuntimeError:
        print("distccd log:")
        sbb.helpers.run.user(args, ["cat", distccd_log], output="stdout",
                             check=False)
        print("sshd log:")
        sshd_log = args.work + "/chroot_native/home/sbos/.distcc-sshd/log.txt"
        sbb.helpers.run.root(args, ["cat", sshd_log], output="stdout",
                             check=False)
        raise
