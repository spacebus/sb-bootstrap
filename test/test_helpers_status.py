# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
""" Test sbb/helpers/status.py """
import os
import pytest
import shutil
import sys

import sbb_test
import sbb_test.git
import sbb.config
import sbb.config.workdir


@pytest.fixture
def args(request):
    import sbb.parse
    sys.argv = ["sbootstrap", "init"]
    args = sbb.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    sbb.helpers.logging.init(args)
    request.addfinalizer(args.logfd.close)
    return args


def test_sbootstrap_status(args, tmpdir):
    """ High level testing of 'sbootstrap status': run it twice, once with
        a fine workdir, and once where one check is failing. """
    # Prepare empty workdir
    work = str(tmpdir)
    with open(work + "/version", "w") as handle:
        handle.write(str(sbb.config.work_version))

    # "sbootstrap status" succeeds (sbb.helpers.run.user verifies exit 0)
    sbootstrap = sbb.config.sbb_src + "/sbootstrap.py"
    sbb.helpers.run.user(args, [sbootstrap, "-w", work, "status",
                                "--details"])

    # Mark chroot_native as outdated
    with open(work + "/workdir.cfg", "w") as handle:
        handle.write("[chroot-init-dates]\nnative = 1234\n")

    # "sbootstrap status" fails
    ret = sbb.helpers.run.user(args, [sbootstrap, "-w", work, "status"],
                               check=False)
    assert ret == 1


def test_print_checks_git_repo(args, monkeypatch, tmpdir):
    """ Test sbb.helpers.status.print_checks_git_repo """
    path, run_git = sbb_test.git.prepare_tmpdir(args, monkeypatch, tmpdir)

    # Not on official branch
    func = sbb.helpers.status.print_checks_git_repo
    name_repo = "test"
    run_git(["checkout", "-b", "inofficial-branch"])
    status, _ = func(args, name_repo)
    assert status == -1

    # Workdir is not clean
    run_git(["checkout", "master"])
    shutil.copy(__file__, path + "/test.py")
    status, _ = func(args, name_repo)
    assert status == -2
    os.unlink(path + "/test.py")

    # Tracking different remote
    status, _ = func(args, name_repo)
    assert status == -3

    # Let master track origin/master
    run_git(["checkout", "-b", "temp"])
    run_git(["branch", "-D", "master"])
    run_git(["checkout", "-b", "master", "--track", "origin/master"])

    # Not up to date
    run_git(["commit", "--allow-empty", "-m", "new"], "remote")
    run_git(["fetch"])
    status, _ = func(args, name_repo)
    assert status == -4

    # Up to date
    run_git(["pull"])
    status, _ = func(args, name_repo)
    assert status == 0

    # Outdated remote information
    def is_outdated(args, path):
        return True
    monkeypatch.setattr(sbb.helpers.git, "is_outdated", is_outdated)
    status, _ = func(args, name_repo)
    assert status == -5
