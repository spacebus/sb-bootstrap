# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import logging
import pytest
import sys
import shutil

import sbb_test  # noqa
import sbb_test.const
import sbb.aportgen
import sbb.config
import sbb.helpers.logging
import sbb.parse


@pytest.fixture
def args(tmpdir, request):
    cfg = f"{sbb_test.const.testdata}/channels.cfg"
    sys.argv = ["sbootstrap.py", "--config-channels", cfg, "build", "-i",
                "device-testsuite-testdevice"]
    args = sbb.parse.arguments()
    args.log = args.work + "/log_testsuite.txt"
    sbb.helpers.logging.init(args)
    request.addfinalizer(args.logfd.close)

    # Fake aports folder:
    tmpdir = str(tmpdir)
    shutil.copytree(args.aports + "/.git", tmpdir + "/.git")
    setattr(args, "_aports_real", args.aports)
    args.aports = tmpdir

    # Copy the devicepkg-dev package (shared device-* APKBUILD code)
    sbb.helpers.run.user(args, ["mkdir", "-p", tmpdir + "/main"])
    path_dev = args._aports_real + "/main/devicepkg-dev"
    sbb.helpers.run.user(args, ["cp", "-r", path_dev, tmpdir + "/main"])

    # Copy the linux-lg-mako aport (we currently copy patches from there)
    sbb.helpers.run.user(args, ["mkdir", "-p", tmpdir + "/device/testing"])
    path_mako = args._aports_real + "/device/testing/linux-lg-mako"
    sbb.helpers.run.user(args, ["cp", "-r", path_mako,
                                f"{tmpdir}/device/testing"])

    # Copy sbaports.cfg
    shutil.copy(f"{sbb_test.const.testdata}/sbaports.cfg", args.aports)
    return args


def generate(args, monkeypatch, answers):
    """
    Generate the device-new-device and linux-new-device aports (with a patched
    sbb.helpers.cli()).

    :returns: (deviceinfo, apkbuild, apkbuild_linux) - the parsed dictionaries
              of the created files, as returned by sbb.parse.apkbuild() and
              sbb.parse.deviceinfo().
    """
    # Patched function
    def fake_ask(args, question="Continue?", choices=["y", "n"], default="n",
                 lowercase_answer=True, validation_regex=None, complete=None):
        for substr, answer in answers.items():
            if substr in question:
                logging.info(question + ": " + answer)
                # raise RuntimeError("test>" + answer)
                return answer
        raise RuntimeError("This testcase didn't expect the question '" +
                           question + "', please add it to the mapping.")

    # Generate the aports
    monkeypatch.setattr(sbb.helpers.cli, "ask", fake_ask)
    sbb.aportgen.generate(args, "device-testsuite-testdevice")
    sbb.aportgen.generate(args, "linux-testsuite-testdevice")
    monkeypatch.undo()

    apkbuild_path = (f"{args.aports}/device/testing/"
                     "device-testsuite-testdevice/APKBUILD")
    apkbuild_path_linux = (args.aports + "/device/testing/"
                           "linux-testsuite-testdevice/APKBUILD")

    # The build fails if the email is not a valid email, so remove them just
    # for tests
    remove_contributor_maintainer_lines(args, apkbuild_path)
    remove_contributor_maintainer_lines(args, apkbuild_path_linux)

    # Parse the deviceinfo and apkbuilds
    args.cache["apkbuild"] = {}
    apkbuild = sbb.parse.apkbuild(args, apkbuild_path)
    apkbuild_linux = sbb.parse.apkbuild(args, apkbuild_path_linux,
                                        check_pkgver=False)
    deviceinfo = sbb.parse.deviceinfo(args, "testsuite-testdevice")
    return (deviceinfo, apkbuild, apkbuild_linux)


def remove_contributor_maintainer_lines(args, path):
    with open(path, "r+", encoding="utf-8") as handle:
        lines_new = []
        for line in handle.readlines():
            # Skip maintainer/contributor
            if line.startswith("# Maintainer") or line.startswith(
                    "# Contributor"):
                continue
            lines_new.append(line)
        # Write back
        handle.seek(0)
        handle.write("".join(lines_new))
        handle.truncate()


def test_aportgen_device_wizard(args, monkeypatch):
    """
    Generate a device-testsuite-testdevice and linux-testsuite-testdevice
    package multiple times and check if the output is correct. Also build the
    device package once.
    """
    # Answers to interactive questions
    answers = {
        "Device architecture": "armv7",
        "external storage": "y",
        "hardware keyboard": "n",
        "Flash method": "heimdall",
        "Manufacturer": "Testsuite",
        "Name": "Testsuite Testdevice",
        "Year": "1337",
        "Chassis": "handset",
        "Type": "isorec",
    }

    # First run
    deviceinfo, apkbuild, apkbuild_linux = generate(args, monkeypatch, answers)
    assert apkbuild["pkgname"] == "device-testsuite-testdevice"
    assert apkbuild["pkgdesc"] == "Testsuite Testdevice"
    assert apkbuild["depends"] == ["linux-testsuite-testdevice",
                                   "mesa-dri-gallium",
                                   "spacebus-base"]

    assert apkbuild_linux["pkgname"] == "linux-testsuite-testdevice"
    assert apkbuild_linux["pkgdesc"] == "Testsuite Testdevice kernel fork"
    assert apkbuild_linux["arch"] == ["armv7"]
    assert apkbuild_linux["_flavor"] == "testsuite-testdevice"

    assert deviceinfo["name"] == "Testsuite Testdevice"
    assert deviceinfo["manufacturer"] == answers["Manufacturer"]
    assert deviceinfo["arch"] == "armv7"
    assert deviceinfo["year"] == "1337"
    assert deviceinfo["chassis"] == "handset"
    assert deviceinfo["keyboard"] == "false"
    assert deviceinfo["external_storage"] == "true"
    assert deviceinfo["flash_method"] == "heimdall-isorec"
    assert deviceinfo["generate_bootimg"] == ""
    assert deviceinfo["generate_legacy_uboot_initfs"] == ""

    # Build the device package
    pkgname = "device-testsuite-testdevice"
    sbb.build.checksum.update(args, pkgname)
    sbb.build.package(args, pkgname, "armv7", force=True)

    # Abort on overwrite confirmation
    answers["overwrite"] = "n"
    with pytest.raises(RuntimeError) as e:
        deviceinfo, apkbuild, apkbuild_linux = generate(args, monkeypatch,
                                                        answers)
    assert "Aborted." in str(e.value)

    # fastboot (mkbootimg)
    answers["overwrite"] = "y"
    answers["Flash method"] = "fastboot"
    answers["Path"] = ""
    deviceinfo, apkbuild, apkbuild_linux = generate(args, monkeypatch, answers)
    assert apkbuild["depends"] == ["linux-testsuite-testdevice",
                                   "mesa-dri-gallium",
                                   "mkbootimg",
                                   "spacebus-base"]

    assert deviceinfo["flash_method"] == answers["Flash method"]
    assert deviceinfo["generate_bootimg"] == "true"

    # 0xffff (legacy uboot initfs)
    answers["Flash method"] = "0xffff"
    deviceinfo, apkbuild, apkbuild_linux = generate(args, monkeypatch, answers)
    assert apkbuild["depends"] == ["linux-testsuite-testdevice",
                                   "mesa-dri-gallium",
                                   "spacebus-base",
                                   "uboot-tools"]

    assert deviceinfo["generate_legacy_uboot_initfs"] == "true"
