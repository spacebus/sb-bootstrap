#! /bin/sh -e
if [ -e "sbootstrap.py" ]; then
	PMB_PATH=$(pwd)
	# shellcheck disable=SC2139
	alias sbbroot="cd \"$PMB_PATH\""
	# shellcheck disable=SC2139
	alias sbootstrap="$PMB_PATH/sbootstrap.py"
else
	echo "ERROR: Please source this from the sbootstrap folder."
	return 1
fi
