#!/usr/bin/env python3

import re
import ast
import sys

from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand

from codecs import open
from os import path


class PyTest(TestCommand):
    user_options = [('pytest-args=', 'a', 'Arguments to pass to pytest')]

    def initialize_options(self):
        TestCommand.initialize_options(self)
        self.pytest_args = ''

    def run_tests(self):
        import shlex
        import pytest
        errno = pytest.main(shlex.split(self.pytest_args))
        sys.exit(errno)


here = path.abspath(path.dirname(__file__))
_version_re = re.compile(r'version\s+=\s+(.*)')

with open(path.join(here, 'sbb/config/__init__.py'), 'rb') as f:
    version = str(ast.literal_eval(_version_re.search(
        f.read().decode('utf-8')).group(1)))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()


setup(
    name='sbootstrap',
    version=version,
    description='A sophisticated chroot / build / flash tool to '
                'develop and install spacebus',
    long_description=long_description,
    long_description_content_type='text/markdown',
    author='spacebus Developers',
    author_email='info@spacebus.org',
    url='https://www.spacebus.org',
    license='GPLv3',
    python_requires='>=3.6',
    classifiers=[
        'Develosbent Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    keywords='spacebus sbootstrap',
    packages=find_packages(exclude=['aports', 'keys', 'test']),
    tests_require=['pytest'],
    cmdclass={'test': PyTest},
    extras_require={
        'completion': ['argcomplete'],
    },
    entry_points={
        'console_scripts': [
            'sbootstrap=sbb:main',
        ],
    },
    include_package_data=True,
)
