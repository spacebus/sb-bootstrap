# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import logging

import sbb.build
import sbb.build.autodetect
import sbb.build.checksum
import sbb.chroot
import sbb.chroot.apk
import sbb.chroot.other
import sbb.helpers.sbaports
import sbb.helpers.run
import sbb.parse


def get_arch(args, apkbuild):
    """
    Take the architecture from the APKBUILD or complain if it's ambiguous. This
    function only gets called if --arch is not set.

    :param apkbuild: looks like: {"pkgname": "linux-...",
                                  "arch": ["x86_64", "armhf", "aarch64"]}
                     or: {"pkgname": "linux-...", "arch": ["armhf"]}
    """
    pkgname = apkbuild["pkgname"]

    # Disabled package (arch="")
    if not apkbuild["arch"]:
        raise RuntimeError(f"'{pkgname}' is disabled (arch=\"\"). Please use"
                           " '--arch' to specify the desired architecture.")

    # Multiple architectures
    if len(apkbuild["arch"]) > 1:
        raise RuntimeError(f"'{pkgname}' supports multiple architectures"
                           f" ({', '.join(apkbuild['arch'])}). Please use"
                           " '--arch' to specify the desired architecture.")

    return apkbuild["arch"][0]


def get_outputdir(args, pkgname, apkbuild):
    """
    Get the folder for the kernel compilation output.
    For most APKBUILDs, this is $builddir. But some older ones still use
    $srcdir/build (see the discussion in #1551).
    """
    # Old style ($srcdir/build)
    ret = "/home/sbos/build/src/build"
    chroot = args.work + "/chroot_native"
    if os.path.exists(chroot + ret + "/.config"):
        logging.warning("*****")
        logging.warning("NOTE: The code in this linux APKBUILD is pretty old."
                        " Consider making a backup and migrating to a modern"
                        " version with: sbootstrap aportgen " + pkgname)
        logging.warning("*****")

        return ret

    # New style ($builddir)
    cmd = "srcdir=/home/sbos/build/src source APKBUILD; echo $builddir"
    ret = sbb.chroot.user(args, ["sh", "-c", cmd],
                          "native", "/home/sbos/build",
                          output_return=True).rstrip()
    if os.path.exists(chroot + ret + "/.config"):
        return ret
    # Some Mediatek kernels use a 'kernel' subdirectory
    if os.path.exists(chroot + ret + "/kernel/.config"):
        return os.path.join(ret, "kernel")

    # Out-of-tree builds ($_outdir)
    if os.path.exists(chroot + ret + "/" + apkbuild["_outdir"] + "/.config"):
        return os.path.join(ret, apkbuild["_outdir"])

    # Not found
    raise RuntimeError("Could not find the kernel config. Consider making a"
                       " backup of your APKBUILD and recreating it from the"
                       " template with: sbootstrap aportgen " + pkgname)


def menuconfig(args, pkgname):
    # Pkgname: allow omitting "linux-" prefix
    if pkgname.startswith("linux-"):
        pkgname_ = pkgname.split("linux-")[1]
        logging.info("PROTIP: You can simply do 'sbootstrap kconfig edit " +
                     pkgname_ + "'")
    else:
        pkgname = "linux-" + pkgname

    # Read apkbuild
    aport = sbb.helpers.sbaports.find(args, pkgname)
    apkbuild = sbb.parse.apkbuild(args, aport + "/APKBUILD")
    arch = args.arch or get_arch(args, apkbuild)
    suffix = sbb.build.autodetect.suffix(args, apkbuild, arch)
    cross = sbb.build.autodetect.crosscompile(args, apkbuild, arch, suffix)
    hostspec = sbb.parse.arch.alpine_to_hostspec(arch)

    # Set up build tools and makedepends
    sbb.build.init(args, suffix)
    if cross:
        sbb.build.init_compiler(args, [], cross, arch)
    depends = apkbuild["makedepends"]
    kopt = "menuconfig"
    copy_xauth = False
    if args.xconfig:
        depends += ["qt5-qtbase-dev", "font-noto"]
        kopt = "xconfig"
        copy_xauth = True
    elif args.nconfig:
        kopt = "nconfig"
        depends += ["ncurses-dev"]
    else:
        depends += ["ncurses-dev"]
    sbb.chroot.apk.install(args, depends)

    # Copy host's .xauthority into native
    if copy_xauth:
        sbb.chroot.other.copy_xauthority(args)

    # Patch and extract sources
    sbb.build.copy_to_buildpath(args, pkgname)
    logging.info("(native) extract kernel source")
    sbb.chroot.user(args, ["abuild", "unpack"], "native", "/home/sbos/build")
    logging.info("(native) apply patches")
    sbb.chroot.user(args, ["abuild", "prepare"], "native",
                    "/home/sbos/build", output="interactive",
                    env={"CARCH": arch})

    # Run make menuconfig
    outputdir = get_outputdir(args, pkgname, apkbuild)
    logging.info("(native) make " + kopt)
    env = {"ARCH": sbb.parse.arch.alpine_to_kernel(arch),
           "DISPLAY": os.environ.get("DISPLAY"),
           "XAUTHORITY": "/home/sbos/.Xauthority"}
    if cross:
        env["CROSS_COMPILE"] = f"{hostspec}-"
        env["CC"] = f"{hostspec}-gcc"
    sbb.chroot.user(args, ["make", kopt], "native",
                    outputdir, output="tui", env=env)

    # Find the updated config
    source = args.work + "/chroot_native" + outputdir + "/.config"
    if not os.path.exists(source):
        raise RuntimeError("No kernel config generated: " + source)

    # Update the aport (config and checksum)
    logging.info("Copy kernel config back to aport-folder")
    config = "config-" + apkbuild["_flavor"] + "." + arch
    target = aport + "/" + config
    sbb.helpers.run.user(args, ["cp", source, target])
    sbb.build.checksum.update(args, pkgname)

    # Check config
    sbb.parse.kconfig.check(args, apkbuild["_flavor"], force_anbox_check=False,
                            force_nftables_check=False,
                            force_containers_check=False,
                            force_zram_check=False,
                            details=True)
