# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import logging

import sbb.chroot
import sbb.build
import sbb.helpers.run
import sbb.helpers.sbaports


def update(args, pkgname):
    """ Fetch all sources and update the checksums in the APKBUILD. """
    sbb.build.init(args)
    sbb.build.copy_to_buildpath(args, pkgname)
    logging.info("(native) generate checksums for " + pkgname)
    sbb.chroot.user(args, ["abuild", "checksum"],
                    working_dir="/home/sbos/build")

    # Copy modified APKBUILD back
    source = args.work + "/chroot_native/home/sbos/build/APKBUILD"
    target = sbb.helpers.sbaports.find(args, pkgname) + "/"
    sbb.helpers.run.user(args, ["cp", source, target])


def verify(args, pkgname):
    """ Fetch all sources and verify their checksums. """
    sbb.build.init(args)
    sbb.build.copy_to_buildpath(args, pkgname)
    logging.info("(native) verify checksums for " + pkgname)

    # Fetch and verify sources, "fetch" alone does not verify them:
    # https://github.com/alpinelinux/abuild/pull/86
    sbb.chroot.user(args, ["abuild", "fetch", "verify"],
                    working_dir="/home/sbos/build")
