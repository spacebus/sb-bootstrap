# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import glob
import os
import logging
import sbb.chroot.user
import sbb.helpers.cli
import sbb.parse


def newapkbuild(args, folder, args_passed, force=False):
    # Initialize build environment and build folder
    sbb.build.init(args)
    build = "/home/sbos/build"
    build_outside = args.work + "/chroot_native" + build
    if os.path.exists(build_outside):
        sbb.chroot.root(args, ["rm", "-r", build])
    sbb.chroot.user(args, ["mkdir", "-p", build])

    # Run newapkbuild
    sbb.chroot.user(args, ["newapkbuild"] + args_passed, working_dir=build)
    glob_result = glob.glob(build_outside + "/*/APKBUILD")
    if not len(glob_result):
        return

    # Paths for copying
    source_apkbuild = glob_result[0]
    pkgname = sbb.parse.apkbuild(args, source_apkbuild, False)["pkgname"]
    target = args.aports + "/" + folder + "/" + pkgname

    # Move /home/sbos/build/$pkgname/* to /home/sbos/build/*
    for path in glob.glob(build_outside + "/*/*"):
        path_inside = build + "/" + pkgname + "/" + os.path.basename(path)
        sbb.chroot.user(args, ["mv", path_inside, build])
    sbb.chroot.user(args, ["rmdir", build + "/" + pkgname])

    # Overwrite confirmation
    if os.path.exists(target):
        logging.warning("WARNING: Folder already exists: " + target)
        question = "Continue and delete its contents?"
        if not force and not sbb.helpers.cli.confirm(args, question):
            raise RuntimeError("Aborted.")
        sbb.helpers.run.user(args, ["rm", "-r", target])

    # Copy the aport (without the extracted src folder)
    logging.info("Create " + target)
    sbb.helpers.run.user(args, ["mkdir", "-p", target])
    for path in glob.glob(build_outside + "/*"):
        if not os.path.isdir(path):
            sbb.helpers.run.user(args, ["cp", path, target])
