# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import sbb.config


def merge_with_args(args):
    """
    We have the internal config (sbb/config/__init__.py) and the user config
    (usually ~/.config/sbootstrap.cfg, can be changed with the '-c'
    parameter).

    Args holds the variables parsed from the commandline (e.g. -j fills out
    args.jobs), and values specified on the commandline count the most.

    In case it is not specified on the commandline, for the keys in
    sbb.config.config_keys, we look into the value set in the the user config.

    When that is empty as well (e.g. just before sbootstrap init), or the key
    is not in sbb.config_keys, we use the default value from the internal
    config.
    """
    # Use defaults from the user's config file
    cfg = sbb.config.load(args)
    for key in cfg["sbootstrap"]:
        if key not in args or getattr(args, key) is None:
            value = cfg["sbootstrap"][key]
            if key in sbb.config.defaults:
                default = sbb.config.defaults[key]
                if isinstance(default, bool):
                    value = (value.lower() == "true")
            setattr(args, key, value)

    # Use defaults from sbb.config.defaults
    for key, value in sbb.config.defaults.items():
        if key not in args or getattr(args, key) is None:
            setattr(args, key, value)
