# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import logging
import configparser
import os
import sbb.config


def load(args):
    cfg = configparser.ConfigParser()
    if os.path.isfile(args.config):
        cfg.read(args.config)

    if "sbootstrap" not in cfg:
        cfg["sbootstrap"] = {}

    for key in sbb.config.defaults:
        if key in sbb.config.config_keys and key not in cfg["sbootstrap"]:
            cfg["sbootstrap"][key] = str(sbb.config.defaults[key])

        # We used to save default values in the config, which can *not* be
        # configured in "sbootstrap init". That doesn't make sense, we always
        # want to use the defaults from sbb/config/__init__.py in that case,
        # not some outdated version we saved some time back (eg. aports folder,
        # spacebus binary packages mirror).
        if key not in sbb.config.config_keys and key in cfg["sbootstrap"]:
            logging.debug("Ignored unconfigurable and possibly outdated"
                          " default value from config:"
                          f" {cfg['sbootstrap'][key]}")
            del cfg["sbootstrap"][key]

    return cfg
