# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import configparser
import logging
import os

import sbb.config
import sbb.helpers.git
import sbb.helpers.sbaports


def check_legacy_folder():
    # Existing sbootstrap/aports must be a symlink
    link = sbb.config.sbb_src + "/aports"
    if os.path.exists(link) and not os.path.islink(link):
        raise RuntimeError("The path '" + link + "' should be a"
                           " symlink pointing to the new sbaports"
                           " repository, which was split from the"
                           " sbootstrap repository (#383). Consider"
                           " making a backup of that folder, then delete"
                           " it and run 'sbootstrap init' again to let"
                           " sbootstrap clone the sbaports repository and"
                           " set up the symlink.")


def clone(args):
    logging.info("Setting up the native chroot and cloning the package build"
                 " recipes (sbaports)...")

    # Set up the native chroot and clone sbaports
    sbb.helpers.git.clone(args, "sbaports")


def symlink(args):
    # Create the symlink
    # This won't work when sbootstrap was installed system wide, but that's
    # okay since the symlink is only intended to make the migration to the
    # sbaports repository easier.
    link = sbb.config.sbb_src + "/aports"
    try:
        os.symlink(args.aports, link)
        logging.info("NOTE: sbaports path: " + link)
    except:
        logging.info("NOTE: sbaports path: " + args.aports)


def check_version_sbaports(real):
    # Compare versions
    min = sbb.config.sbaports_min_version
    if sbb.parse.version.compare(real, min) >= 0:
        return

    # Outated error
    logging.info("NOTE: your sbaports folder has version " + real + ", but" +
                 " version " + min + " is required.")
    raise RuntimeError("Run 'sbootstrap pull' to update your sbaports.")


def check_version_sbootstrap(min):
    # Compare versions
    real = sbb.config.version
    if sbb.parse.version.compare(real, min) >= 0:
        return

    # Show versions
    logging.info("NOTE: you are using sbootstrap version " + real + ", but" +
                 " version " + min + " is required.")

    # Error for git clone
    sbb_src = sbb.config.sbb_src
    if os.path.exists(sbb_src + "/.git"):
        raise RuntimeError("Please update your local sbootstrap repository."
                           " Usually with: 'git -C \"" + sbb_src + "\" pull'")

    # Error for package manager installation
    raise RuntimeError("Please update your sbootstrap version (with your"
                       " distribution's package manager, or with pip, "
                       " depending on how you have installed it). If that is"
                       " not possible, consider cloning the latest version"
                       " of sbootstrap from git.")


def read_config(args):
    """ Read and verify sbaports.cfg. """
    # Try cache first
    cache_key = "sbb.config.sbaports.read_config"
    if args.cache[cache_key]:
        return args.cache[cache_key]

    # Migration message
    if not os.path.exists(args.aports):
        raise RuntimeError("We have split the aports repository from the"
                           " sbootstrap repository (#383). Please run"
                           " 'sbootstrap init' again to clone it.")

    # Require the config
    path_cfg = args.aports + "/sbaports.cfg"
    if not os.path.exists(path_cfg):
        raise RuntimeError("Invalid sbaports repository, could not find the"
                           " config: " + path_cfg)

    # Load the config
    cfg = configparser.ConfigParser()
    cfg.read(path_cfg)
    ret = cfg["sbaports"]

    # Version checks
    check_version_sbaports(ret["version"])
    check_version_sbootstrap(ret["sbootstrap_min_version"])

    # Translate legacy channel names
    ret["channel"] = sbb.helpers.sbaports.get_channel_new(ret["channel"])

    # Cache and return
    args.cache[cache_key] = ret
    return ret


def read_config_channel(args):
    """ Get the properties of the currently active channel in sbaports.git,
        as specified in channels.cfg (https://spacebus.org/channels.cfg).
        :returns: {"description: ...,
                   "branch_sbaports": ...,
                   "branch_aports": ...,
                   "mirrordir_alpine": ...} """
    channel = read_config(args)["channel"]
    channels_cfg = sbb.helpers.git.parse_channels_cfg(args)

    if channel in channels_cfg["channels"]:
        return channels_cfg["channels"][channel]

    # Channel not in channels.cfg, try to be helpful
    branch = sbb.helpers.git.rev_parse(args, args.aports,
                                       extra_args=["--abbrev-ref"])
    branches_official = sbb.helpers.git.get_branches_official(args, "sbaports")
    branches_official = ", ".join(branches_official)
    remote = sbb.helpers.git.get_upstream_remote(args, "sbaports")
    logging.info("NOTE: fix the error by rebasing or cherry picking relevant"
                 " commits from this branch onto a branch that is on a"
                 f" supported channel: {branches_official}")
    logging.info("NOTE: as workaround, you may pass --config-channels with a"
                 " custom channels.cfg. Reference:"
                 " https://spacebus.org/channels.cfg")
    raise RuntimeError(f"Current branch '{branch}' of sbaports.git is on"
                       f" channel '{channel}', but this channel was not"
                       f" found in channels.cfg (of {remote}/master"
                       " branch). Looks like a very old branch.")


def init(args):
    check_legacy_folder()
    if not os.path.exists(args.aports):
        clone(args)
    symlink(args)
    read_config(args)


def switch_to_channel_branch(args, channel_new):
    """ Checkout the channel's branch in sbaports.git.
        :channel_new: channel name (e.g. "edge", "v21.03")
        :returns: True if another branch was checked out, False otherwise """
    # Check current sbaports branch channel
    channel_current = read_config(args)["channel"]
    if channel_current == channel_new:
        return False

    # List current and new branches/channels
    channels_cfg = sbb.helpers.git.parse_channels_cfg(args)
    branch_new = channels_cfg["channels"][channel_new]["branch_sbaports"]
    branch_current = sbb.helpers.git.rev_parse(args, args.aports,
                                               extra_args=["--abbrev-ref"])
    logging.info(f"Currently checked out branch '{branch_current}' of"
                 f" sbaports.git is on channel '{channel_current}'.")
    logging.info(f"Switching to branch '{branch_new}' on channel"
                 f" '{channel_new}'...")

    # Make sure we don't have mounts related to the old channel
    sbb.chroot.shutdown(args)

    # Attempt to switch branch (git gives a nice error message, mentioning
    # which files need to be committed/stashed, so just pass it through)
    if sbb.helpers.run.user(args, ["git", "checkout", branch_new],
                            args.aports, "interactive", check=False):
        raise RuntimeError("Failed to switch branch. Go to your sbaports and"
                           " fix what git complained about, then try again: "
                           f"{args.aports}")

    # Invalidate all caches
    sbb.helpers.args.add_cache(args)

    # Verify sbaports.cfg on new branch
    read_config(args)
    return True
