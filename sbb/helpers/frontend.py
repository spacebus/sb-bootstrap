# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import glob
import json
import logging
import os
import sys

import sbb.aportgen
import sbb.build
import sbb.build.autodetect
import sbb.sideload
import sbb.chroot
import sbb.chroot.initfs
import sbb.chroot.other
import sbb.config
import sbb.export
import sbb.flasher
import sbb.helpers.devices
import sbb.helpers.git
import sbb.helpers.lint
import sbb.helpers.logging
import sbb.helpers.pkgrel_bump
import sbb.helpers.sbaports
import sbb.helpers.repo
import sbb.helpers.repo_missing
import sbb.helpers.run
import sbb.helpers.aportupgrade
import sbb.helpers.status
import sbb.install
import sbb.install.blockdevice
import sbb.parse
import sbb.qemu


def _parse_flavor(args, autoinstall=True):
    """
    Verify the flavor argument if specified, or return a default value.
    :param autoinstall: make sure that at least one kernel flavor is installed
    """
    # Install at least one kernel and get installed flavors
    suffix = "rootfs_" + args.device
    flavors = sbb.chroot.other.kernel_flavors_installed(
        args, suffix, autoinstall)

    # Parse and verify the flavor argument
    flavor = args.flavor
    if flavor:
        if flavor not in flavors:
            raise RuntimeError(f"No kernel installed with flavor {flavor}!" +
                               " Run 'sbootstrap flasher list_flavors' to"
                               " get a list.")
        return flavor
    if not len(flavors):
        raise RuntimeError(
            "No kernel flavors installed in chroot " + suffix + "! Please let"
            " your device package depend on a package starting with 'linux-'.")
    return flavors[0]


def _parse_suffix(args):
    if "rootfs" in args and args.rootfs:
        return "rootfs_" + args.device
    elif args.buildroot:
        if args.buildroot == "device":
            return "buildroot_" + args.deviceinfo["arch"]
        else:
            return "buildroot_" + args.buildroot
    elif args.suffix:
        return args.suffix
    else:
        return "native"


def _install_ondev_verify_no_rootfs(args):
    chroot_dest = "/var/lib/rootfs.img"
    dest = f"{args.work}/chroot_installer_{args.device}{chroot_dest}"
    if os.path.exists(dest):
        return

    if args.ondev_cp:
        for _, chroot_dest_cp in args.ondev_cp:
            if chroot_dest_cp == chroot_dest:
                return

    raise ValueError(f"--no-rootfs set, but rootfs.img not found in install"
                     " chroot. Either run 'sbootstrap install' without"
                     " --no-rootfs first to let it generate the spacebus"
                     " rootfs once, or supply a rootfs file with:"
                     f" --cp os.img:{chroot_dest}")


def aportgen(args):
    for package in args.packages:
        logging.info("Generate aport: " + package)
        sbb.aportgen.generate(args, package)


def build(args):
    # Strict mode: zap everything
    if args.strict:
        sbb.chroot.zap(args, False)

    if args.envkernel:
        sbb.build.envkernel.package_kernel(args)
        return

    # Set src and force
    src = os.path.realpath(os.path.expanduser(args.src[0])) \
        if args.src else None
    force = True if src else args.force
    if src and not os.path.exists(src):
        raise RuntimeError("Invalid path specified for --src: " + src)

    # Build all packages
    for package in args.packages:
        arch_package = args.arch or sbb.build.autodetect.arch(args, package)
        if not sbb.build.package(args, package, arch_package, force,
                                 args.strict, src=src):
            logging.info("NOTE: Package '" + package + "' is up to date. Use"
                         " 'sbootstrap build " + package + " --force'"
                         " if needed.")


def build_init(args):
    suffix = _parse_suffix(args)
    sbb.build.init(args, suffix)


def checksum(args):
    for package in args.packages:
        if args.verify:
            sbb.build.checksum.verify(args, package)
        else:
            sbb.build.checksum.update(args, package)


def sideload(args):
    arch = args.deviceinfo["arch"]
    if args.arch:
        arch = args.arch
    user = args.user
    host = args.host
    sbb.sideload.sideload(args, user, host, args.port, arch, args.install_key,
                          args.packages)


def chroot(args):
    # Suffix
    suffix = _parse_suffix(args)
    if (args.user and suffix != "native" and
            not suffix.startswith("buildroot_")):
        raise RuntimeError("--user is only supported for native or"
                           " buildroot_* chroots.")
    if args.xauth and suffix != "native":
        raise RuntimeError("--xauth is only supported for native chroot.")

    # apk: check minimum version, install packages
    sbb.chroot.apk.check_min_version(args, suffix)
    if args.add:
        sbb.chroot.apk.install(args, args.add.split(","), suffix)

    # Xauthority
    env = {}
    if args.xauth:
        sbb.chroot.other.copy_xauthority(args)
        env["DISPLAY"] = os.environ.get("DISPLAY")
        env["XAUTHORITY"] = "/home/sbos/.Xauthority"

    # Install blockdevice
    if args.install_blockdev:
        size_boot = 128  # 128 MiB
        size_root = 4096  # 4 GiB
        size_reserve = 2048  # 2 GiB
        sbb.install.blockdevice.create_and_mount_image(args, size_boot,
                                                       size_root, size_reserve)

    # Run the command as user/root
    if args.user:
        logging.info("(" + suffix + ") % su sbos -c '" +
                     " ".join(args.command) + "'")
        sbb.chroot.user(args, args.command, suffix, output=args.output,
                        env=env)
    else:
        logging.info("(" + suffix + ") % " + " ".join(args.command))
        sbb.chroot.root(args, args.command, suffix, output=args.output,
                        env=env)


def config(args):
    keys = sbb.config.config_keys
    if args.name and args.name not in keys:
        logging.info("NOTE: Valid config keys: " + ", ".join(keys))
        raise RuntimeError("Invalid config key: " + args.name)

    cfg = sbb.config.load(args)
    if args.reset:
        if args.name is None:
            raise RuntimeError("config --reset requires a name to be given.")
        value = sbb.config.defaults[args.name]
        cfg["sbootstrap"][args.name] = value
        logging.info(f"Config changed to default: {args.name}='{value}'")
        sbb.config.save(args, cfg)
    elif args.value is not None:
        cfg["sbootstrap"][args.name] = args.value
        logging.info("Config changed: " + args.name + "='" + args.value + "'")
        sbb.config.save(args, cfg)
    elif args.name:
        value = cfg["sbootstrap"].get(args.name, "")
        print(value)
    else:
        cfg.write(sys.stdout)

    # Don't write the "Done" message
    sbb.helpers.logging.disable()


def repo_missing(args):
    missing = sbb.helpers.repo_missing.generate(args, args.arch, args.overview,
                                                args.package, args.built)
    print(json.dumps(missing, indent=4))


def index(args):
    sbb.build.index_repo(args)


def initfs(args):
    sbb.chroot.initfs.frontend(args)


def install(args):
    if args.no_fde:
        logging.warning("WARNING: --no-fde is deprecated,"
                        " as it is now the default.")
    if args.rsync and args.full_disk_encryption:
        raise ValueError("Installation using rsync is not compatible with full"
                         " disk encryption.")
    if args.rsync and not args.sdcard:
        raise ValueError("Installation using rsync only works on sdcard.")

    # On-device installer checks
    # Note that this can't be in the mutually exclusive group that has most of
    # the conflicting options, because then it would not work with --sdcard.
    if args.on_device_installer:
        if args.full_disk_encryption:
            raise ValueError("--on-device-installer cannot be combined with"
                             " --fde. The user can choose to encrypt their"
                             " installation later in the on-device installer.")
        if args.android_recovery_zip:
            raise ValueError("--on-device-installer cannot be combined with"
                             " --android-recovery-zip (patches welcome)")
        if args.no_image:
            raise ValueError("--on-device-installer cannot be combined with"
                             " --no-image")
        if args.rsync:
            raise ValueError("--on-device-installer cannot be combined with"
                             " --rsync")
        if args.filesystem:
            raise ValueError("--on-device-installer cannot be combined with"
                             " --filesystem")
    else:
        if args.ondev_cp:
            raise ValueError("--cp can only be combined with --ondev")
        if args.ondev_no_rootfs:
            raise ValueError("--no-rootfs can only be combined with --ondev."
                             " Do you mean --no-image?")
    if args.ondev_no_rootfs:
        _install_ondev_verify_no_rootfs(args)

    # On-device installer overrides
    if args.on_device_installer:
        # To make code for the on-device installer not needlessly complex, just
        # hardcode "user" as username here. (The on-device installer will set
        # a password for the user, disable SSH password authentication,
        # optionally add a new user for SSH that must not have the same
        # username etc.)
        if args.user != "user":
            logging.warning(f"WARNING: custom username '{args.user}' will be"
                            " replaced with 'user' for the on-device"
                            " installer.")
            args.user = "user"

    if not args.sdcard and args.split is None:
        # Default to split if the flash method requires it
        flasher = sbb.config.flashers.get(args.deviceinfo["flash_method"], {})
        if flasher.get("split", False):
            args.split = True

    # Android recovery zip related
    if args.android_recovery_zip and args.filesystem:
        raise ValueError("--android-recovery-zip cannot be combined with"
                         " --filesystem (patches welcome)")
    if args.android_recovery_zip and args.full_disk_encryption:
        logging.info("WARNING: --fde is rarely used in combination with"
                     " --android-recovery-zip. If this does not work, consider"
                     " using another method (e.g. installing via netcat)")
        logging.info("WARNING: the kernel of the recovery system (e.g. TWRP)"
                     f" must support the cryptsetup cipher '{args.cipher}'.")
        logging.info("If you know what you are doing, consider setting a"
                     " different cipher with 'sbootstrap install --cipher=..."
                     " --fde --android-recovery-zip'.")

    # Don't install locally compiled packages and package signing keys
    if not args.install_local_pkgs:
        # Implies that we don't build outdated packages (overriding the answer
        # in 'sbootstrap init')
        args.build_pkgs_on_install = False

        # Safest way to avoid installing local packages is having none
        if glob.glob(f"{args.work}/packages/*"):
            raise ValueError("--no-local-pkgs specified, but locally built"
                             " packages found. Consider 'sbootstrap zap -p'"
                             " to delete them.")

    # Verify that the root filesystem is supported by current sbaports branch
    sbb.install.get_root_filesystem(args)

    sbb.install.install(args)


def flasher(args):
    sbb.flasher.frontend(args)


def export(args):
    sbb.export.frontend(args)


def update(args):
    existing_only = not args.non_existing
    if not sbb.helpers.repo.update(args, args.arch, True, existing_only):
        logging.info("No APKINDEX files exist, so none have been updated."
                     " The sbootstrap command downloads the APKINDEX files on"
                     " demand.")
        logging.info("If you want to force downloading the APKINDEX files for"
                     " all architectures (not recommended), use:"
                     " sbootstrap update --non-existing")


def newapkbuild(args):
    # Check for SRCURL usage
    is_url = False
    for prefix in ["http://", "https://", "ftp://"]:
        if args.pkgname_pkgver_srcurl.startswith(prefix):
            is_url = True
            break

    # Sanity check: -n is only allowed with SRCURL
    if args.pkgname and not is_url:
        raise RuntimeError("You can only specify a pkgname (-n) when using"
                           " SRCURL as last parameter.")

    # Passthrough: Strings (e.g. -d "my description")
    pass_through = []
    for entry in sbb.config.newapkbuild_arguments_strings:
        value = getattr(args, entry[1])
        if value:
            pass_through += [entry[0], value]

    # Passthrough: Switches (e.g. -C for CMake)
    for entry in (sbb.config.newapkbuild_arguments_switches_pkgtypes +
                  sbb.config.newapkbuild_arguments_switches_other):
        if getattr(args, entry[1]) is True:
            pass_through.append(entry[0])

    # Passthrough: PKGNAME[-PKGVER] | SRCURL
    pass_through.append(args.pkgname_pkgver_srcurl)
    sbb.build.newapkbuild(args, args.folder, pass_through, args.force)


def kconfig(args):
    if args.action_kconfig == "check":
        # Handle passing a file directly
        if args.file:
            if sbb.parse.kconfig.check_file(args, args.package,
                                            anbox=args.anbox,
                                            nftables=args.nftables,
                                            containers=args.containers,
                                            zram=args.zram,
                                            details=True):
                logging.info("kconfig check succeeded!")
                return
            raise RuntimeError("kconfig check failed!")

        # Default to all kernel packages
        packages = []
        if args.package:
            packages = [args.package]
        else:
            for aport in sbb.helpers.sbaports.get_list(args):
                if aport.startswith("linux-"):
                    packages.append(aport.split("linux-")[1])

        # Iterate over all kernels
        error = False
        skipped = 0
        packages.sort()
        for package in packages:
            if not args.force:
                pkgname = package if package.startswith("linux-") \
                    else "linux-" + package
                aport = sbb.helpers.sbaports.find(args, pkgname)
                apkbuild = sbb.parse.apkbuild(args, aport + "/APKBUILD")
                if "!sbb:kconfigcheck" in apkbuild["options"]:
                    skipped += 1
                    continue
            if not sbb.parse.kconfig.check(
                    args, package,
                    force_anbox_check=args.anbox,
                    force_nftables_check=args.nftables,
                    force_containers_check=args.containers,
                    force_zram_check=args.zram,
                    details=True):
                error = True

        # At least one failure
        if error:
            raise RuntimeError("kconfig check failed!")
        else:
            if skipped:
                logging.info("NOTE: " + str(skipped) + " kernel(s) was skipped"
                             " (consider 'sbootstrap kconfig check -f')")
            logging.info("kconfig check succeeded!")
    elif args.action_kconfig == "edit":
        if args.package:
            pkgname = args.package
        else:
            pkgname = args.deviceinfo["codename"]
        sbb.build.menuconfig(args, pkgname)


def deviceinfo_parse(args):
    # Default to all devices
    devices = args.devices
    if not devices:
        devices = sbb.helpers.devices.list_codenames(args)

    # Iterate over all devices
    kernel = args.deviceinfo_parse_kernel
    for device in devices:
        print(f"{device}, with kernel={kernel}:")
        print(json.dumps(sbb.parse.deviceinfo(args, device, kernel), indent=4,
                         sort_keys=True))


def apkbuild_parse(args):
    # Default to all packages
    packages = args.packages
    if not packages:
        packages = sbb.helpers.sbaports.get_list(args)

    # Iterate over all packages
    for package in packages:
        print(package + ":")
        aport = sbb.helpers.sbaports.find(args, package)
        path = aport + "/APKBUILD"
        print(json.dumps(sbb.parse.apkbuild(args, path), indent=4,
                         sort_keys=True))


def apkindex_parse(args):
    result = sbb.parse.apkindex.parse(args, args.apkindex_path)
    if args.package:
        if args.package not in result:
            raise RuntimeError("Package not found in the APKINDEX: " +
                               args.package)
        result = result[args.package]
    print(json.dumps(result, indent=4))


def pkgrel_bump(args):
    would_bump = True
    if args.auto:
        would_bump = sbb.helpers.pkgrel_bump.auto(args, args.dry)
    else:
        # Each package must exist
        for package in args.packages:
            sbb.helpers.sbaports.find(args, package)

        # Increase pkgrel
        for package in args.packages:
            sbb.helpers.pkgrel_bump.package(args, package, dry=args.dry)

    if args.dry and would_bump:
        logging.info("Pkgrels of package(s) would have been bumped!")
        sys.exit(1)


def aportupgrade(args):
    if args.all or args.all_stable or args.all_git:
        sbb.helpers.aportupgrade.upgrade_all(args)
    else:
        # Each package must exist
        for package in args.packages:
            sbb.helpers.sbaports.find(args, package)

        # Check each package for a new version
        for package in args.packages:
            sbb.helpers.aportupgrade.upgrade(args, package)


def qemu(args):
    sbb.qemu.run(args)


def shutdown(args):
    sbb.chroot.shutdown(args)


def stats(args):
    # Chroot suffix
    suffix = "native"
    if args.arch != args.arch_native:
        suffix = "buildroot_" + args.arch

    # Install ccache and display stats
    sbb.chroot.apk.install(args, ["ccache"], suffix)
    logging.info("(" + suffix + ") % ccache -s")
    sbb.chroot.user(args, ["ccache", "-s"], suffix, output="stdout")


def work_migrate(args):
    # do nothing (sbb/__init__.py already did the migration)
    sbb.helpers.logging.disable()


def log(args):
    if args.clear_log:
        sbb.helpers.run.user(args, ["truncate", "-s", "0", args.log])
    sbb.helpers.run.user(args, ["tail", "-n", args.lines, "-F", args.log],
                         output="tui")


def log_distccd(args):
    logpath = "/home/sbos/distccd.log"
    if args.clear_log:
        sbb.chroot.user(args, ["truncate", "-s", "0", logpath])
    sbb.chroot.user(args, ["tail", "-n", args.lines, "-f", logpath],
                    output="tui")


def zap(args):
    sbb.chroot.zap(args, dry=args.dry, http=args.http,
                   distfiles=args.distfiles, pkgs_local=args.pkgs_local,
                   pkgs_local_mismatch=args.pkgs_local_mismatch,
                   pkgs_online_mismatch=args.pkgs_online_mismatch,
                   rust=args.rust)

    # Don't write the "Done" message
    sbb.helpers.logging.disable()


def bootimg_analyze(args):
    bootimg = sbb.parse.bootimg(args, args.path)
    tmp_output = "Put these variables in the deviceinfo file of your device:\n"
    for line in sbb.aportgen.device.\
            generate_deviceinfo_fastboot_content(args, bootimg).split("\n"):
        tmp_output += "\n" + line.lstrip()
    logging.info(tmp_output)


def pull(args):
    failed = []
    for repo in sbb.config.git_repos.keys():
        if sbb.helpers.git.pull(args, repo) < 0:
            failed.append(repo)

    if not failed:
        return True

    logging.info("---")
    logging.info("WARNING: failed to update: " + ", ".join(failed))
    logging.info("")
    logging.info("'sbootstrap pull' will only update the repositories, if:")
    logging.info("* they are on an officially supported branch (e.g. master)")
    logging.info("* the history is not conflicting (fast-forward is possible)")
    logging.info("* the git workdirs are clean")
    logging.info("You have changed mentioned repositories, so they don't meet")
    logging.info("these conditions anymore.")
    logging.info("")
    logging.info("Fix and try again:")
    for name_repo in failed:
        logging.info("* " + sbb.helpers.git.get_path(args, name_repo))
    logging.info("---")
    return False


def lint(args):
    packages = args.packages
    if not packages:
        packages = sbb.helpers.sbaports.get_list(args)

    for package in packages:
        sbb.helpers.lint.check(args, package)


def status(args):
    if not sbb.helpers.status.print_status(args, args.details):
        sys.exit(1)
