# Copyright 2021 Danct12 <danct12@disroot.org>
# SPDX-License-Identifier: GPL-3.0-or-later
import logging

import sbb.chroot
import sbb.chroot.apk
import sbb.build
import sbb.helpers.run
import sbb.helpers.sbaports


def check(args, pkgname):
    sbb.chroot.apk.install(args, ["atools"])

    # Run apkbuild-lint on copy of sbaport in chroot
    sbb.build.init(args)
    sbb.build.copy_to_buildpath(args, pkgname)
    logging.info("(native) linting " + pkgname + " with apkbuild-lint")
    options = sbb.config.apkbuild_custom_valid_options
    return sbb.chroot.user(args, ["apkbuild-lint", "APKBUILD"],
                           check=False, output="stdout",
                           output_return=True,
                           working_dir="/home/sbos/build",
                           env={"CUSTOM_VALID_OPTIONS": " ".join(options)})
