# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import logging
import sbb.aportgen.binutils
import sbb.aportgen.busybox_static
import sbb.aportgen.device
import sbb.aportgen.gcc
import sbb.aportgen.linux
import sbb.aportgen.musl
import sbb.aportgen.grub_efi
import sbb.config
import sbb.helpers.cli


def properties(pkgname):
    """
    Get the `sbb.config.aportgen` properties for the aport generator, based on
    the pkgname prefix.

    Example: "musl-armhf" => ("musl", "cross", {"confirm_overwrite": False})

    :param pkgname: package name
    :returns: (prefix, folder, options)
    """
    for folder, options in sbb.config.aportgen.items():
        for prefix in options["prefixes"]:
            if pkgname.startswith(prefix):
                return (prefix, folder, options)
    logging.info("NOTE: aportgen is for generating spacebus specific"
                 " aports, such as the cross-compiler related packages"
                 " or the linux kernel fork packages.")
    logging.info("NOTE: If you wanted to package new software in general, try"
                 " 'sbootstrap newapkbuild' to generate a template.")
    raise ValueError("No generator available for " + pkgname + "!")


def generate(args, pkgname):
    if args.fork_alpine:
        prefix, folder, options = (pkgname, "temp",
                                   {"confirm_overwrite": True})
    else:
        prefix, folder, options = properties(pkgname)
    path_target = args.aports + "/" + folder + "/" + pkgname

    # Confirm overwrite
    if options["confirm_overwrite"] and os.path.exists(path_target):
        logging.warning("WARNING: Target folder already exists: "
                        f"{path_target}")
        if not sbb.helpers.cli.confirm(args, "Continue and overwrite?"):
            raise RuntimeError("Aborted.")

    if os.path.exists(args.work + "/aportgen"):
        sbb.helpers.run.user(args, ["rm", "-r", args.work + "/aportgen"])
    if args.fork_alpine:
        upstream = sbb.aportgen.core.get_upstream_aport(args, pkgname)
        sbb.helpers.run.user(args, ["cp", "-r", upstream,
                                    f"{args.work}/aportgen"])
        sbb.aportgen.core.rewrite(args, pkgname, replace_simple={
            "# Contributor:*": None, "# Maintainer:*": None})
    else:
        # Run sbb.aportgen.PREFIX.generate()
        getattr(sbb.aportgen, prefix.replace("-", "_")).generate(args, pkgname)

    # Move to the aports folder
    if os.path.exists(path_target):
        sbb.helpers.run.user(args, ["rm", "-r", path_target])
    sbb.helpers.run.user(
        args, ["mv", args.work + "/aportgen", path_target])

    logging.info("*** sbaport generated: " + path_target)
