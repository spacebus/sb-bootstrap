# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import logging
import shlex

import sbb.chroot
import sbb.config
import sbb.helpers.apk
import sbb.helpers.sbaports
import sbb.parse.apkindex
import sbb.parse.arch
import sbb.parse.depends
import sbb.parse.version


def update_repository_list(args, suffix="native", check=False):
    """
    Update /etc/apk/repositories, if it is outdated (when the user changed the
    --mirror-alpine or --mirror-sbOS parameters).

    :param check: This function calls it self after updating the
                  /etc/apk/repositories file, to check if it was successful.
                  Only for this purpose, the "check" parameter should be set to
                  True.
    """
    # Skip if we already did this
    if suffix in args.cache["apk_repository_list_updated"]:
        return

    # Read old entries or create folder structure
    path = f"{args.work}/chroot_{suffix}/etc/apk/repositories"
    lines_old = []
    if os.path.exists(path):
        # Read all old lines
        lines_old = []
        with open(path) as handle:
            for line in handle:
                lines_old.append(line[:-1])
    else:
        sbb.helpers.run.root(args, ["mkdir", "-p", os.path.dirname(path)])

    # Up to date: Save cache, return
    lines_new = sbb.helpers.repo.urls(args)
    if lines_old == lines_new:
        args.cache["apk_repository_list_updated"].append(suffix)
        return

    # Check phase: raise error when still outdated
    if check:
        raise RuntimeError(f"Failed to update: {path}")

    # Update the file
    logging.debug(f"({suffix}) update /etc/apk/repositories")
    if os.path.exists(path):
        sbb.helpers.run.root(args, ["rm", path])
    for line in lines_new:
        sbb.helpers.run.root(args, ["sh", "-c", "echo "
                                    f"{shlex.quote(line)} >> {path}"])
    update_repository_list(args, suffix, True)


def check_min_version(args, suffix="native"):
    """
    Check the minimum apk version, before running it the first time in the
    current session (lifetime of one sbootstrap call).
    """

    # Skip if we already did this
    if suffix in args.cache["apk_min_version_checked"]:
        return

    # Skip if apk is not installed yet
    if not os.path.exists(f"{args.work}/chroot_{suffix}/sbin/apk"):
        logging.debug(f"NOTE: Skipped apk version check for chroot '{suffix}'"
                      ", because it is not installed yet!")
        return

    # Compare
    version_installed = installed(args, suffix)["apk-tools"]["version"]
    sbb.helpers.apk.check_outdated(
        args, version_installed,
        "Delete your http cache and zap all chroots, then try again:"
        " 'sbootstrap zap -hc'")

    # Mark this suffix as checked
    args.cache["apk_min_version_checked"].append(suffix)


def install_is_necessary(args, build, arch, package, packages_installed):
    """
    This function optionally builds an out of date package, and checks if the
    version installed inside a chroot is up to date.
    :param build: Set to true to build the package, if the binary packages are
                  out of date, and it is in the aports folder.
    :param packages_installed: Return value from installed().
    :returns: True if the package needs to be installed/updated,
              False otherwise.
    """
    # User may have disabled buiding packages during "sbootstrap install"
    build_disabled = False
    if args.action == "install" and not args.build_pkgs_on_install:
        build_disabled = True

    # Build package
    if build and not build_disabled:
        sbb.build.package(args, package, arch)

    # No further checks when not installed
    if package not in packages_installed:
        return True

    # Make sure, that we really have a binary package
    data_repo = sbb.parse.apkindex.package(args, package, arch, False)
    if not data_repo:
        if build_disabled:
            raise RuntimeError(f"{package}: no binary package found for"
                               f" {arch}, and compiling packages during"
                               " 'sbootstrap install' has been disabled."
                               " Consider changing this option in"
                               " 'sbootstrap init'.")
        logging.warning("WARNING: Internal error in sbootstrap,"
                        f" package '{package}' for {arch}"
                        " has not been built yet, but it should have"
                        " been. Rebuilding it with force. Please "
                        " report this, if there is no ticket about this"
                        " yet!")
        sbb.build.package(args, package, arch, True)
        return install_is_necessary(args, build, arch, package,
                                    packages_installed)

    # Compare the installed version vs. the version in the repos
    data_installed = packages_installed[package]
    compare = sbb.parse.version.compare(data_installed["version"],
                                        data_repo["version"])
    # a) Installed newer (should not happen normally)
    if compare == 1:
        logging.info(f"WARNING: {arch} package '{package}'"
                     f" installed version {data_installed['version']}"
                     " is newer, than the version in the repositories:"
                     f" {data_repo['version']}"
                     " See also: <https://spacebus.org/warning-repo>")
        return False

    # b) Repo newer
    elif compare == -1:
        return True

    # c) Same version, look at last modified
    elif compare == 0:
        time_installed = float(data_installed["timestamp"])
        time_repo = float(data_repo["timestamp"])
        return time_repo > time_installed


def replace_aports_packages_with_path(args, packages, suffix, arch):
    """
    apk will only re-install packages with the same pkgname,
    pkgver and pkgrel, when you give it the absolute path to the package.
    This function replaces all packages, that were built locally,
    with the absolute path to the package.
    """
    ret = []
    for package in packages:
        aport = sbb.helpers.sbaports.find(args, package, False)
        if aport:
            data_repo = sbb.parse.apkindex.package(args, package, arch, False)
            if not data_repo:
                raise RuntimeError(f"{package}: could not find binary"
                                   " package, although it should exist for"
                                   " sure at this point in the code."
                                   " Probably an APKBUILD subpackage parsing"
                                   " bug. Related: https://gitlab.com/"
                                   "spacebus/build.spacebus.org/"
                                   "issues/61")
            apk_path = (f"/mnt/sbootstrap-packages/{arch}/"
                        f"{package}-{data_repo['version']}.apk")
            if os.path.exists(f"{args.work}/chroot_{suffix}{apk_path}"):
                package = apk_path
        ret.append(package)
    return ret


def install(args, packages, suffix="native", build=True):
    """
    :param build: automatically build the package, when it does not exist yet
                  or needs to be updated, and it is inside the sb-aports
                  folder. Checking this is expensive - if you know, that all
                  packages are provides by upstream repos, set this to False!
    """
    # Initialize chroot
    check_min_version(args, suffix)
    sbb.chroot.init(args, suffix)

    # Add depends to packages
    arch = sbb.parse.arch.from_chroot_suffix(args, suffix)
    packages_with_depends = sbb.parse.depends.recurse(args, packages, suffix)

    # Filter outdated packages (build them if required)
    packages_installed = installed(args, suffix)
    packages_todo = []
    for package in packages_with_depends:
        if install_is_necessary(
                args, build, arch, package, packages_installed):
            packages_todo.append(package)
    if not len(packages_todo):
        return

    # Sanitize packages: don't allow '--allow-untrusted' and other options
    # to be passed to apk!
    for package in packages_todo:
        if package.startswith("-"):
            raise ValueError(f"Invalid package name: {package}")

    # Readable install message without dependencies
    message = f"({suffix}) install"
    for pkgname in packages:
        if pkgname not in packages_installed:
            message += f" {pkgname}"
    logging.info(message)

    # Local packages: Using the path instead of pkgname makes apk update
    # packages of the same version if the build date is different
    packages_todo = replace_aports_packages_with_path(args, packages_todo,
                                                      suffix, arch)

    # Use a virtual package to mark only the explicitly requested packages as
    # explicitly installed, not their dependencies or specific paths (#1212)
    commands = [["add"] + packages]
    if packages != packages_todo:
        commands = [["add", "-u", "--virtual", ".sbootstrap"] + packages_todo,
                    ["add"] + packages,
                    ["del", ".sbootstrap"]]
    for (i, command) in enumerate(commands):
        if args.offline:
            command = ["--no-network"] + command
        if i == 0:
            sbb.helpers.apk.apk_with_progress(args, ["apk"] + command,
                                              chroot=True, suffix=suffix)
        else:
            # Virtual package related commands don't actually install or remove
            # packages, but only mark the right ones as explicitly installed.
            # They finish up almost instantly, so don't display a progress bar.
            sbb.chroot.root(args, ["apk", "--no-progress"] + command,
                            suffix=suffix)


def installed(args, suffix="native"):
    """
    Read the list of installed packages (which has almost the same format, as
    an APKINDEX, but with more keys).

    :returns: a dictionary with the following structure:
              { "spacebus-mkinitfs":
                {
                  "pkgname": "spacebus-mkinitfs"
                  "version": "0.0.4-r10",
                  "depends": ["busybox-extras", "lddtree", ...],
                  "provides": ["mkinitfs=0.0.1"]
                }, ...
              }
    """
    path = f"{args.work}/chroot_{suffix}/lib/apk/db/installed"
    return sbb.parse.apkindex.parse(args, path, False)
