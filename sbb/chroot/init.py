# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import logging
import os
import glob
import filecmp

import sbb.chroot
import sbb.chroot.apk_static
import sbb.config
import sbb.config.workdir
import sbb.helpers.repo
import sbb.helpers.run
import sbb.parse.arch


def copy_resolv_conf(args, suffix="native"):
    """
    Use pythons super fast file compare function (due to caching)
    and copy the /etc/resolv.conf to the chroot, in case it is
    different from the host.
    If the file doesn't exist, create an empty file with 'touch'.
    """
    host = "/etc/resolv.conf"
    chroot = f"{args.work}/chroot_{suffix}{host}"
    if os.path.exists(host):
        if not os.path.exists(chroot) or not filecmp.cmp(host, chroot):
            sbb.helpers.run.root(args, ["cp", host, chroot])
    else:
        sbb.helpers.run.root(args, ["touch", chroot])


def mark_in_chroot(args, suffix="native"):
    """
    Touch a flag so we can know when we're running in chroot (and
    don't accidentally flash partitions on our host)
    """
    in_chroot_file = f"{args.work}/chroot_{suffix}/in-sbootstrap"
    if not os.path.exists(in_chroot_file):
        sbb.helpers.run.root(args, ["touch", in_chroot_file])


def setup_qemu_emulation(args, suffix):
    arch = sbb.parse.arch.from_chroot_suffix(args, suffix)
    if not sbb.parse.arch.cpu_emulation_required(args, arch):
        return

    chroot = f"{args.work}/chroot_{suffix}"
    arch_qemu = sbb.parse.arch.alpine_to_qemu(arch)

    # mount --bind the qemu-user binary
    sbb.chroot.binfmt.register(args, arch)
    sbb.helpers.mount.bind_file(args, f"{args.work}/chroot_native"
                                      f"/usr/bin/qemu-{arch_qemu}",
                                f"{chroot}/usr/bin/qemu-{arch_qemu}-static",
                                create_folders=True)


def init(args, suffix="native"):
    # When already initialized: just prepare the chroot
    chroot = f"{args.work}/chroot_{suffix}"
    arch = sbb.parse.arch.from_chroot_suffix(args, suffix)

    sbb.chroot.mount(args, suffix)
    setup_qemu_emulation(args, suffix)
    mark_in_chroot(args, suffix)
    if os.path.islink(f"{chroot}/bin/sh"):
        sbb.config.workdir.chroot_check_channel(args, suffix)
        copy_resolv_conf(args, suffix)
        sbb.chroot.apk.update_repository_list(args, suffix)
        return

    # Require apk-tools-static
    sbb.chroot.apk_static.init(args)

    logging.info(f"({suffix}) install alpine-base")

    # Initialize cache
    apk_cache = f"{args.work}/cache_apk_{arch}"
    sbb.helpers.run.root(args, ["ln", "-s", "-f", "/var/cache/apk",
                                f"{chroot}/etc/apk/cache"])

    # Initialize /etc/apk/keys/, resolv.conf, repositories
    for key in glob.glob(f"{sbb.config.apk_keys_path}/*.pub"):
        sbb.helpers.run.root(args, ["cp", key, f"{args.work}"
                                               "/config_apk_keys/"])
    copy_resolv_conf(args, suffix)
    sbb.chroot.apk.update_repository_list(args, suffix)

    sbb.config.workdir.chroot_save_init(args, suffix)

    # Install alpine-base
    sbb.helpers.repo.update(args, arch)
    sbb.chroot.apk_static.run(args, ["--root", chroot,
                                     "--cache-dir", apk_cache,
                                     "--initdb", "--arch", arch,
                                     "add", "alpine-base"])

    # Building chroots: create "sbos" user, add symlinks to /home/sbos
    if not suffix.startswith("rootfs_"):
        sbb.chroot.root(args, ["adduser", "-D", "sbos", "-u",
                               sbb.config.chroot_uid_user],
                        suffix, auto_init=False)

        # Create the links (with subfolders if necessary)
        for target, link_name in sbb.config.chroot_home_symlinks.items():
            link_dir = os.path.dirname(link_name)
            if not os.path.exists(f"{chroot}{link_dir}"):
                sbb.chroot.user(args, ["mkdir", "-p", link_dir], suffix)
            if not os.path.exists(f"{chroot}{target}"):
                sbb.chroot.root(args, ["mkdir", "-p", target], suffix)
            sbb.chroot.user(args, ["ln", "-s", target, link_name], suffix)
            sbb.chroot.root(args, ["chown", "sbos:sbos", target], suffix)
