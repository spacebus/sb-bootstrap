# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import logging
import sbb.chroot.initfs_hooks
import sbb.chroot.other
import sbb.chroot.apk
import sbb.helpers.cli


def build(args, flavor, suffix):
    # Update mkinitfs and hooks
    sbb.chroot.apk.install(args, ["spacebus-mkinitfs"], suffix)
    sbb.chroot.initfs_hooks.update(args, suffix)

    # Call mkinitfs
    logging.info(f"({suffix}) mkinitfs {flavor}")
    release_file = (f"{args.work}/chroot_{suffix}/usr/share/kernel/"
                    f"{flavor}/kernel.release")
    with open(release_file, "r") as handle:
        release = handle.read().rstrip()
    sbb.chroot.root(args, ["mkinitfs", "-o",
                           f"/boot/initramfs-{flavor}", release],
                    suffix)


def extract(args, flavor, suffix, extra=False):
    """
    Extract the initramfs to /tmp/initfs-extracted or the initramfs-extra to
    /tmp/initfs-extra-extracted and return the outside extraction path.
    """
    # Extraction folder
    inside = "/tmp/initfs-extracted"
    if extra:
        inside = "/tmp/initfs-extra-extracted"
        flavor += "-extra"
    outside = f"{args.work}/chroot_{suffix}{inside}"
    if os.path.exists(outside):
        if not sbb.helpers.cli.confirm(args, f"Extraction folder {outside}"
                                       " already exists."
                                       " Do you want to overwrite it?"):
            raise RuntimeError("Aborted!")
        sbb.chroot.root(args, ["rm", "-r", inside], suffix)

    # Extraction script (because passing a file to stdin is not allowed
    # in sbootstrap's chroot/shell functions for security reasons)
    with open(f"{args.work}/chroot_{suffix}/tmp/_extract.sh", "w") as handle:
        handle.write(
            "#!/bin/sh\n"
            f"cd {inside} && cpio -i < _initfs\n")

    # Extract
    commands = [["mkdir", "-p", inside],
                ["cp", f"/boot/initramfs-{flavor}", f"{inside}/_initfs.gz"],
                ["gzip", "-d", f"{inside}/_initfs.gz"],
                ["cat", "/tmp/_extract.sh"],  # for the log
                ["sh", "/tmp/_extract.sh"],
                ["rm", "/tmp/_extract.sh", f"{inside}/_initfs"]
                ]
    for command in commands:
        sbb.chroot.root(args, command, suffix)

    # Return outside path for logging
    return outside


def ls(args, flavor, suffix, extra=False):
    tmp = "/tmp/initfs-extracted"
    if extra:
        tmp = "/tmp/initfs-extra-extracted"
    extract(args, flavor, suffix, extra)
    sbb.chroot.root(args, ["ls", "-lahR", "."], suffix, tmp, "stdout")
    sbb.chroot.root(args, ["rm", "-r", tmp], suffix)


def frontend(args):
    # Find the appropriate kernel flavor
    suffix = f"rootfs_{args.device}"
    flavors = sbb.chroot.other.kernel_flavors_installed(args, suffix)
    flavor = flavors[0]
    if hasattr(args, "flavor") and args.flavor:
        flavor = args.flavor

    # Handle initfs actions
    action = args.action_initfs
    if action == "build":
        build(args, flavor, suffix)
    elif action == "extract":
        dir = extract(args, flavor, suffix)
        logging.info(f"Successfully extracted initramfs to: {dir}")
        dir_extra = extract(args, flavor, suffix, True)
        logging.info(f"Successfully extracted initramfs-extra to: {dir_extra}")
    elif action == "ls":
        logging.info("*** initramfs ***")
        ls(args, flavor, suffix)
        logging.info("*** initramfs-extra ***")
        ls(args, flavor, suffix, True)

    # Handle hook actions
    elif action == "hook_ls":
        sbb.chroot.initfs_hooks.ls(args, suffix)
    else:
        if action == "hook_add":
            sbb.chroot.initfs_hooks.add(args, args.hook, suffix)
        elif action == "hook_del":
            sbb.chroot.initfs_hooks.delete(args, args.hook, suffix)

        # Rebuild the initfs for all kernels after adding/removing a hook
        for flavor in flavors:
            build(args, flavor, suffix)

    if action in ["ls", "extract"]:
        link = "https://wiki.spacebus.org/wiki/Initramfs_develosbent"
        logging.info(f"See also: <{link}>")
