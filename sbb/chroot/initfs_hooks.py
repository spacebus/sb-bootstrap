# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import glob
import logging

import sbb.config
import sbb.chroot.apk


def list_chroot(args, suffix, remove_prefix=True):
    ret = []
    prefix = sbb.config.initfs_hook_prefix
    for pkgname in sbb.chroot.apk.installed(args, suffix).keys():
        if pkgname.startswith(prefix):
            if remove_prefix:
                ret.append(pkgname[len(prefix):])
            else:
                ret.append(pkgname)
    return ret


def list_aports(args):
    ret = []
    prefix = sbb.config.initfs_hook_prefix
    for path in glob.glob(f"{args.aports}/*/{prefix}*"):
        ret.append(os.path.basename(path)[len(prefix):])
    return ret


def ls(args, suffix):
    hooks_chroot = list_chroot(args, suffix)
    hooks_aports = list_aports(args)

    for hook in hooks_aports:
        line = f"* {hook} ({'' if hook in hooks_chroot else 'not '}installed)"
        logging.info(line)


def add(args, hook, suffix):
    if hook not in list_aports(args):
        raise RuntimeError("Invalid hook name!"
                           " Run 'sbootstrap initfs hook_ls'"
                           " to get a list of all hooks.")
    prefix = sbb.config.initfs_hook_prefix
    sbb.chroot.apk.install(args, [f"{prefix}{hook}"], suffix)


def delete(args, hook, suffix):
    if hook not in list_chroot(args, suffix):
        raise RuntimeError("There is no such hook installed!")
    prefix = sbb.config.initfs_hook_prefix
    sbb.chroot.root(args, ["apk", "del", f"{prefix}{hook}"], suffix)


def update(args, suffix):
    """
    Rebuild and update all hooks, that are out of date
    """
    sbb.chroot.apk.install(args, list_chroot(args, suffix, False), suffix)
