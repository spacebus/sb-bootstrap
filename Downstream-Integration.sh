#!/usr/bin/bash

find . -type f -print0 | xargs -0 sed -i 's/sbb/sbb/g'

find . -type f -print0 | xargs -0 sed -i 's/spacebus/spacebus/g'

find . -type f -print0 | xargs -0 sed -i 's/spacebus.org/spacebus.org/g'

find . -type f -print0 | xargs -0 sed -i 's/sbbootstrap/sbootstrap/g'

find . -type f -print0 | xargs -0 sed -i 's/sb/sb/g'
mv test/sbb_test test/sbb_test 

mv sbb sbb 

mv test/testdata/sbb_groups test/testdata/sbb_groups 

mv test/testdata/sbb_install test/testdata/sbb_install 
mv test/testdata/sbb_recommends test/testdata/sbb_recommends 

mv sbb/config/pmaports.py sbb/config/sbaports.py
mv sbb/helpers/pmaports.py sbb/helpers/sbaports.py
mv test/test_config_pmaports.py test/test_config_sbaports.py
mv test/test_helpers_pmaports.py test/test_helpers_sbaports.py
mv test/testdata/aportgen/pmaports test/testdata/aportgen/sbaports
mv test/testdata/helpers_ui/pmaports test/testdata/helpers_ui/sbaports
mv test/testdata/helpers_ui/pmaports/sbaports.cfg test/testdata/helpers_ui/sbaports/sbaports.cfg
mv test/testdata/pmaports.cfg test/testdata/sbaports.cfg

