# sbootstrap
[**Introduction**](https://spacebus.org/blog/2017/05/26/intro/) | [**Security Warning**](https://ollieparanoid.github.io/post/security-warning/) | [**Devices**](https://wiki.spacebus.org/wiki/Devices)

Sophisticated chroot/build/flash tool to develop and install [spacebus](https://spacebus.org).

Package build scripts live in the [`sbaports`](https://gitlab.com/spacebus/sbaports) repository now.

## Requirements
* 2 GB of RAM recommended for compiling
* Linux distribution on the host system (`x86`, `x86_64`, or `aarch64`)
  * [Windows subsystem for Linux (WSL)](https://en.wikipedia.org/wiki/Windows_Subsystem_for_Linux) does **not** work! Please use [VirtualBox](https://www.virtualbox.org/) instead.
  * Kernels based on the grsec patchset [do **not** work](https://github.com/spacebus/sbootstrap/issues/107) *(Alpine: use linux-vanilla instead of linux-hardened, Arch: linux-hardened [is not based on grsec](https://www.reddit.com/r/archlinux/comments/68b2jn/linuxhardened_in_community_repo_a_grsecurity/))*
  * On Alpine Linux only: `apk add coreutils procps`
  * [Linux kernel 3.17 or higher](https://spacebus.org/oldkernel)
* Python 3.6+
* OpenSSL
* git

## Usage Examples
Please refer to the [spacebus wiki](https://wiki.spacebus.org) for in-depth coverage of topics such as [porting to a new device](https://wiki.spacebus.org/wiki/Porting_to_a_new_device) or [installation](https://wiki.spacebus.org/wiki/Installation_guide). The help output (`sbootstrap -h`) has detailed usage instructions for every command. Read on for some generic examples of what can be done with `sbootstrap`.

### Installing sbootstrap
<https://wiki.spacebus.org/wiki/Installing_sbootstrap>

### Basics
Initial setup:
```
$ sbootstrap init
```

Run this in a second window to see all shell commands that get executed:
```
$ sbootstrap log
```

Quick health check and config overview:
```
$ sbootstrap status
```

### Packages
Build `aports/main/hello-world`:
```
$ sbootstrap build hello-world
```

Cross-compile to `armhf`:
```
$ sbootstrap build --arch=armhf hello-world
```

Build with source code from local folder:
```
$ sbootstrap build linux-spacebus-mainline --src=~/code/linux
```

Update checksums:
```
$ sbootstrap checksum hello-world
```

Generate a template for a new package:
```
$ sbootstrap newapkbuild "https://gitlab.com/spacebus/osk-sdl/-/archive/0.52/osk-sdl-0.52.tar.bz2"
```

### Chroots
Enter the `armhf` building chroot:
```
$ sbootstrap chroot -b armhf
```

Run a command inside a chroot:
```
$ sbootstrap chroot -- echo test
```

Safely delete all chroots:
```
$ sbootstrap zap
```

### Device Porting Assistance
Analyze Android [`boot.img`](https://wiki.spacebus.org/wiki/Glossary#boot.img) files (also works with recovery OS images like TWRP):
```
$ sbootstrap bootimg_analyze ~/Downloads/twrp-3.2.1-0-fp2.img
```

Check kernel configs:
```
$ sbootstrap kconfig check
```

Edit a kernel config:
```
$ sbootstrap kconfig edit --arch=armhf spacebus-mainline
```

### Root File System
Build the rootfs:
```
$ sbootstrap install
```

Build the rootfs with full disk encryption:
```
$ sbootstrap install --fde
```

Update existing installation on SD card:
```
$ sbootstrap install --sdcard=/dev/mmcblk0 --rsync
```

Run the image in QEMU:
```
$ sbootstrap qemu --image-size=1G
```

Flash to the device:
```
$ sbootstrap flasher flash_kernel
$ sbootstrap flasher flash_rootfs --partition=userdata
```

Export the rootfs, kernel, initramfs, `boot.img` etc.:
```
$ sbootstrap export
```

Extract the initramfs
```
$ sbootstrap initfs extract
```

Build and flash Android recovery zip:
```
$ sbootstrap install --android-recovery-zip
$ sbootstrap flasher --method=adb sideload
```

### Repository Maintenance
List sbaports that don't have a binary package:
```
$ sbootstrap repo_missing --arch=armhf --overview
```

Increase the `pkgrel` for each aport where the binary package has outdated dependencies (e.g. after soname bumps):
```
$ sbootstrap pkgrel_bump --auto
```

Generate cross-compiler aports based on the latest version from Alpine's aports:
```
$ sbootstrap aportgen binutils-armhf gcc-armhf
```

Manually rebuild package index:
```
$ sbootstrap index
```

Delete local binary packages without existing aport of same version:
```
$ sbootstrap zap -m
```

### Debugging
Use `-v` on any action to get verbose logging:
```
$ sbootstrap -v build hello-world
```

Parse a single deviceinfo and return it as JSON:
```
$ sbootstrap deviceinfo_parse pine64-pinephone
```

Parse a single APKBUILD and return it as JSON:
```
$ sbootstrap apkbuild_parse hello-world
```

Parse a package from an APKINDEX and return it as JSON:
```
$ sbootstrap apkindex_parse $WORK/cache_apk_x86_64/APKINDEX.8b865e19.tar.gz hello-world
```

`ccache` statistics:
```
$ sbootstrap stats --arch=armhf
```

`distccd` log:
```
$ sbootstrap log_distccd
```

## Develosbent
### Requirements for running tests
* [Shellcheck](https://shellcheck.net/)

You also need to install the following python packages (pip can be useful if you distribution hasn't got them packaged):
* `pytest`
* `pytest-cov` (only needed for `test/testcases_fast.sh`, which is what CI runs)
* `flake8`

On Alpine Linux it can be done with:
```shell
$ sudo apk add grep shellcheck py3-pytest py3-pytest-cov py3-flake8
```

### Running linters
The easiest way is to run the same script CI runs:
```shell
$ ./test/static_code_analysis.sh
```

### Running tests
You can now run `pytest -vv` inside the sbootstrap folder to run all available tests.

CI runs slightly reduces set of tests (it skips tests that require running qemu) by this:
```shell
$ ./test/testcases_fast.sh
```
This is the easiest way to do the same as CI.

Alternatively you can run a single test if you wish:
```shell
$ pytest -vv ./test/test_keys.py
```

## License
[GPLv3](LICENSE)
